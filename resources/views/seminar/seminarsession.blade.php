@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Penilaian Seminar</div>
                    <div class="panel-body">
                        <div class="center">
                            <ul class="nav nav-pills">
                                <li class="active"><a data-toggle="pill" href="#pembimbing">Pembimbing</a></li>
                                <li><a data-toggle="pill" href="#penguji">Penguji</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="pembimbing" class="tab-pane fade in active">
                                <form method="POST" action="/seminarsession/update">
                                    {!! csrf_field() !!}
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th class="text-center">Parameter</th>
                                                <th class="text-center">{!! $name[0] !!}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($param1 as $i => $param)
                                                <tr>
                                                    <td class="text-center">{!! $param->NamaParameter !!}</td>
                                                    <td class="text-center">
                                                        @if ($pembimbing[$i]->DosenID == $user)
                                                            <select name="value_select[]">
                                                                <!-- set default value based on stored data; initialized value is M -->
                                                                @if ($pembimbing[$i]->ParamValue === 0)
                                                                    <option value="0" selected>L</option>
                                                                    <option value="1">M</option>
                                                                    <option value="2">K</option>
                                                                @elseif ($pembimbing[$i]->ParamValue === 1)
                                                                    <option value="0">L</option>
                                                                    <option value="1" selected>M</option>
                                                                    <option value="2">K</option>
                                                                @else
                                                                    <option value="0">L</option>
                                                                    <option value="1">M</option>
                                                                    <option value="2" selected>K</option>
                                                                @endif
                                                            </select>
                                                        @else
                                                            @if ($pembimbing[$i]->ParamValue === 0)
                                                                L
                                                            @elseif ($pembimbing[$i]->ParamValue === 1)
                                                                M
                                                            @else
                                                                K
                                                            @endif
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td class="text-center">Nilai Pembimbing</td>
                                                @foreach ($nilai as $n)
                                                    @if ($n->DosenID == $info->DosenAID)
                                                        <td class="text-center">
                                                            @if ($n->ParamValue === 0)
                                                                <label>L</label>
                                                            @elseif ($n->ParamValue === 1)
                                                                <label>M</label>
                                                            @else
                                                                <label>K</label>
                                                            @endif
                                                        </td>
                                                    @endif
                                                @endforeach
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <input type="hidden" id="seminar_id" name="seminar_id" value="{{ $id }}">
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="indeks" class="col-md-3 control-label">Indeks</label>
                                            <div class="col-md-6">
                                                <label>{!! $indeks !!}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="catatan" class="col-md-3 control-label">Catatan</label>
                                            <div class="col-md-6">
                                                <textarea id="catatan" class="form-control" rows="5"
                                                          name="catatan">{!! $catatan !!}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <input type="hidden" id="tipe" name="tipe" value="_0">
                                    @if ($info->DosenAID == $user)
                                        <input type="submit" value="Simpan Perubahan">
                                    @endif
                                </form>
                                @foreach ($param1 as $p)
                                    @if ($p->ChildSum == 0)
                                        <br>
                                        <div class="row">
                                            <div class="form-group">
                                                <label for="nama_param" class="col-md-3 control-label">Nama
                                                    Parameter</label>
                                                <div class="col-md-6">
                                                    <label id="nama_param" class="col-md-9" rows="2" name="nama_param">{!! $p->NamaParameter !!}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="form-group">
                                                <p class="col-md-3">Keterangan L</p>
                                                <div class="col-md-6">
                                                    <label id="deskripsi_L" class="form-control" rows="4"
                                                           name="deskripsi_L">{!! $p->Deskripsi_L !!}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="form-group">
                                                <p class="col-md-3">Keterangan M</p>
                                                <div class="col-md-6">
                                                    <label id="deskripsi_M" class="form-control" rows="4"
                                                           name="deskripsi_M">{!! $p->Deskripsi_M !!}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="form-group">
                                                <p class="col-md-3">Keterangan K</p>
                                                <div class="col-md-6">
                                                    <label id="deskripsi_K" class="form-control" rows="4"
                                                           name="deskripsi_K">{!! $p->Deskripsi_K !!}</label>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <div id="penguji" class="tab-pane fade">
                                <form method="POST" action="/seminarsession/update">
                                    {!! csrf_field() !!}
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th class="text-center">Parameter</th>
                                                <th class="text-center">{!! $name[1] !!}</th>
                                                <th class="text-center">{!! $name[2] !!}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($param2 as $i => $param)
                                                <tr>
                                                    <td class="text-center">{!! $param->NamaParameter !!}</td>
                                                    <td class="text-center">
                                                        @if ($penguji1[$i]->DosenID == $user)
                                                            <select name="value_select[]">
                                                                <!-- set default value based on stored data; initialized value is M -->
                                                                @if ($penguji1[$i]->ParamValue === 0)
                                                                    <option value="0" selected>L</option>
                                                                    <option value="1">M</option>
                                                                    <option value="2">K</option>
                                                                @elseif ($penguji1[$i]->ParamValue === 1)
                                                                    <option value="0">L</option>
                                                                    <option value="1" selected>M</option>
                                                                    <option value="2">K</option>
                                                                @else
                                                                    <option value="0">L</option>
                                                                    <option value="1">M</option>
                                                                    <option value="2" selected>K</option>
                                                                @endif
                                                            </select>
                                                        @else
                                                            @if ($penguji1[$i]->ParamValue === 0)
                                                                L
                                                            @elseif ($penguji1[$i]->ParamValue === 1)
                                                                M
                                                            @else
                                                                K
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        @if ($penguji2[$i]->DosenID == $user)
                                                            <select name="value_select[]">
                                                                <!-- set default value based on stored data; initialized value is M -->
                                                                @if ($penguji2[$i]->ParamValue === 0)
                                                                    <option value="0" selected>L</option>
                                                                    <option value="1">M</option>
                                                                    <option value="2">K</option>
                                                                @elseif ($penguji2[$i]->ParamValue === 1)
                                                                    <option value="0">L</option>
                                                                    <option value="1" selected>M</option>
                                                                    <option value="2">K</option>
                                                                @else
                                                                    <option value="0">L</option>
                                                                    <option value="1">M</option>
                                                                    <option value="2" selected>K</option>
                                                                @endif
                                                            </select>
                                                        @else
                                                            @if ($penguji2[$i]->ParamValue === 0)
                                                                L
                                                            @elseif ($penguji2[$i]->ParamValue === 1)
                                                                M
                                                            @else
                                                                K
                                                            @endif
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td class="text-center">Nilai Penguji</td>
                                                @foreach ($nilai as $n)
                                                    @if ($n->DosenID == $info->DosenBID)
                                                        <td class="text-center">
                                                            @if ($n->ParamValue === 0)
                                                                <label>L</label>
                                                            @elseif ($n->ParamValue === 1)
                                                                <label>M</label>
                                                            @else
                                                                <label>K</label>
                                                            @endif
                                                        </td>
                                                    @endif
                                                @endforeach
                                                @foreach ($nilai as $n)
                                                    @if ($n->DosenID == $info->DosenCID)
                                                        <td class="text-center">
                                                            @if ($n->ParamValue === 0)
                                                                <label>L</label>
                                                            @elseif ($n->ParamValue === 1)
                                                                <label>M</label>
                                                            @else
                                                                <label>K</label>
                                                            @endif
                                                        </td>
                                                    @endif
                                                @endforeach
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <input type="hidden" id="seminar_id" name="seminar_id" value="{{ $id }}">
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="indeks" class="col-md-3 control-label">Indeks</label>
                                            <div class="col-md-6">
                                                <label>{!! $indeks !!}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="catatan" class="col-md-3 control-label">Catatan</label>
                                            <div class="col-md-6">
                                                <textarea id="catatan" class="form-control" rows="5"
                                                          name="catatan">{!! $catatan !!}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <input type="hidden" id="tipe" name="tipe" value="_1">
                                    @if ($info->DosenBID == $user || $info->DosenCID == $user)
                                        <input type="submit" value="Simpan Perubahan">
                                    @endif
                                </form>
                                @foreach ($param2 as $p)
                                    @if ($p->ChildSum == 0)
                                        <br>
                                        <div class="row">
                                            <div class="form-group">
                                                <label for="nama_param" class="col-md-3 control-label">Nama Parameter</label>
                                                <div class="col-md-6">
                                                    <label id="nama_param" class="col-md-9" rows="2" name="nama_param">{!! $p->NamaParameter !!}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="form-group">
                                                <p class="col-md-3">Keterangan L</p>
                                                <div class="col-md-6">
                                                    <label id="deskripsi_L" class="form-control" rows="4"
                                                           name="deskripsi_L">{!! $p->Deskripsi_L !!}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="form-group">
                                                <p class="col-md-3">Keterangan M</p>
                                                <div class="col-md-6">
                                                    <label id="deskripsi_M" class="form-control" rows="4"
                                                           name="deskripsi_M">{!! $p->Deskripsi_M !!}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="form-group">
                                                <p class="col-md-3">Keterangan K</p>
                                                <div class="col-md-6">
                                                    <label id="deskripsi_K" class="form-control" rows="4"
                                                           name="deskripsi_K">{!! $p->Deskripsi_K !!}</label>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection