@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Detil Seminar</div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table ui-jq="dataTable" class="table table-hover">
                                <thead>
                                <tr>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">NIM Mahasiswa</th>
                                    <th class="text-center">NIP Dosen Pembimbing</th>
                                    <th class="text-center">NIP Dosen Penguji 1</th>
                                    <th class="text-center">NIP Dosen Penguji 2</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Indeks</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($list as $info)
                                    <tr>
                                        <td class="text-center">{!! $info->ID !!}</td>
                                        <td class="text-center">{!! $info->MahasiswaID !!}</td>
                                        <td class="text-center">{!! $info->DosenAID !!}</td>
                                        <td class="text-center">{!! $info->DosenBID !!}</td>
                                        <td class="text-center">{!! $info->DosenCID !!}</td>
                                        <td class="text-center">
                                            @if (Auth::user()->role == 'Kaprodi')
                                                @if ($info->Active === 1) Berlangsung
                                                @else Tidak Berlangsung
                                                @endif
                                            @else
                                                @if ($info->Active === 1) <a
                                                        href="{{ url('changestate', ['id' => $info->SeminarID]) }}">Berlangsung</a>
                                                @else <a href="{{ url('changestate', ['id' => $info->SeminarID]) }}">Tidak
                                                    Berlangsung</a>
                                                @endif
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            {!! $info->Nilai !!}
                                        </td>
                                        <td class="text-center">
                                            @if ($info->Active == 0)
                                                <a href="{{ url('seminardetail', ['id' => $info->SeminarID]) }}">Detail</a>
                                            @else
                                                Detail
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection