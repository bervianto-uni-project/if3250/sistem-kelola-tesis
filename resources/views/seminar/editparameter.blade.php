@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if( ! empty($errorText))
                    <div class="alert alert-danger">
                        <strong>Edit failed!</strong> {{ $errorText }}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>
                            <a href="{{ url('daftarparameter') }}"> <i class="material-icons">navigate_before</i></a>
                            Edit Parameter
                        </h2>
                    </div>

                    <div class="panel-body">
                        <form method="POST" action="/editparameter/submit">
                            {!! csrf_field() !!}
                            <div class="field_wrapper">
                                @if ($next != null || $id != '0')
                                    <div class="row">
                                        <div class="col-md-4 btn-group" role="group">
                                            <a class="btn btn-default"
                                               href="{{ url('editparameter', ['tipe' => $tipe, 'id' => $next[0]]) }}">
                                                Add Parameter
                                            </a>
                                            <a class="btn btn-default"
                                               href="{{ url('editparameter', ['tipe' => $tipe, 'id' => $next[1]]) }}">
                                                Add Sub Parameter
                                            </a>
                                        </div>
                                    </div>
                                @endif

                                <div class="row top-buffer">
                                    <div class="form-group">
                                        <label for="nama_param" class="col-md-3 control-label">Nama Parameter</label>
                                        <div class="col-md-6">
                                        <textarea id="nama_param" class="form-control" name="nama_param"
                                                  placeholder="<?php if ($info != null) {
                                                      echo trim($info->NamaParameter);
                                                  }?>"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row top-buffer">
                                    <div class="form-group">
                                        <label for="deskripsi_L" class="col-md-3 control-label">Keterangan L</label>
                                        <div class="col-md-6">
                                        <textarea id="deskripsi_L" class="form-control" rows="3" name="deskripsi_L"
                                                  placeholder="<?php if ($info != null) {
                                                      echo trim($info->Deskripsi_L);
                                                  }?>"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row top-buffer">
                                    <div class="form-group">
                                        <label for="deskripsi_M" class="col-md-3 control-label">Keterangan M</label>
                                        <div class="col-md-6">
                                        <textarea id="deskripsi_M" class="form-control" rows="3" name="deskripsi_M"
                                                  placeholder="<?php if ($info != null) {
                                                      echo trim($info->Deskripsi_M);
                                                  }?>"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row top-buffer">
                                    <div class="form-group">
                                        <label for="deskripsi_K" class="col-md-3 control-label">Keterangan K</label>
                                        <div class="col-md-6">
                                        <textarea id="deskripsi_K" class="form-control" rows="3" name="deskripsi_K"
                                                  placeholder="<?php if ($info != null) {
                                                      echo trim($info->Deskripsi_K);
                                                  }?>"></textarea>
                                        </div>
                                    </div>
                                </div>
                                @if ($sum > 0)
                                    <table class="table table-bordered top-buffer" id="tab_logic">
                                        <thead>
                                        <tr>
                                                @foreach ($title as $t)
                                                    <td>{!! $t !!}</td>
                                                @endforeach
                                            <td>Nilai Akhir</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if ($outputs != null)
                                            @foreach($outputs as $i => $output) <!-- output newest parameter set -->
                                            <tr id="addr{!! $i !!}" >
                                                @for ($j = 0; $j < $sum; $j++)
                                                    <?php $out = $output["Kriteria"]; ?>
                                                    <td>
                                                        <select name="param{!! $j + 1 !!}[]" class='form-control'>
                                                        @if ($j < count($out))
                                                            <?php $val = $out[$j]; ?>
                                                            @if ($val == 0)
                                                                    <option value='0' selected>{!! $option[0]->NamaParameter !!}</option>
                                                                    <option value='1'>{!! $option[1]->NamaParameter !!}</option>
                                                                    <option value='2'>{!! $option[2]->NamaParameter !!}</option>
                                                            @elseif ($val == 1)
                                                                    <option value='0'>{!! $option[0]->NamaParameter !!}</option>
                                                                    <option value='1' selected>{!!$option[1]->NamaParameter !!}</option>
                                                                    <option value='2'>{!! $option[2]->NamaParameter !!}</option>
                                                            @elseif ($val == 2)
                                                                    <option value='0'>{!!$option[0]->NamaParameter !!}</option>
                                                                    <option value='1'>{!!$option[1]->NamaParameter !!}</option>
                                                                    <option value='2' selected>{!! $option[2]->NamaParameter !!}</option>
                                                            @endif
                                                        @else
                                                                <option value='0'>{!! $option[0]->NamaParameter !!}</option>
                                                                <option value='1'>{!! $option[1]->NamaParameter !!}</option>
                                                                <option value='2'>{!! $option[2]->NamaParameter !!}</option>
                                                        @endif
                                                        </select>
                                                    </td>
                                                @endfor
                                                <td>
                                                    <select name="result[]" class='form-control'>
                                                        <option value='0'>{!! $option[0]->NamaParameter !!}</option>
                                                        <option value='1'>{!! $option[1]->NamaParameter !!}</option>
                                                        <option value='2'>{!! $option[2]->NamaParameter !!}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            @endforeach
                                        @else
                                            <tr id="addr0" >
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <div class="row top-buffer left-margin right-margin bottom-margin">
                                        <button id="add_row" type="button" class="pull-left btn btn-success">Tambah Parameter</button>
                                        <button id='delete_row' type="button" class="pull-right btn btn-danger">Hapus Parameter</button>
                                    </div>
                                @endif
                            </div>
                            <input type="hidden" id="param_id" name="param_id" value="{!! $id !!}">
                            <input type="hidden" id="tipe" name="tipe" value="{!! $tipe !!}">
                            <input type="hidden" id="form_num" name="form_num" value="{!! count($outputs) !!}">
                            <hr>
                            <input type="submit" class="pull-left btn btn-success" value="Simpan">
                        </form>
                        @if ($check == true)
                        <form method="POST" action="/editparameter/delete">
                            {!! csrf_field() !!}
                            <input type="hidden" id="param_id" name="param_id" value="{!! $id !!}">
                            <input type="hidden" id="tipe" name="tipe" value="{!! $tipe !!}">
                            <input type="submit" class="pull-right btn btn-danger" value="Hapus">
                        </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js-files')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
@endsection

@section('page-js-script')
    <script type="text/javascript">
        $(document).ready(function () {
            var i = $('#form_num').val()
            var sum = parseInt(" <?php echo $sum ?> ");
            var opt = new Array();
            <?php foreach($option as $o){ ?>
                opt.push('<?php echo $o->NamaParameter; ?>');
            <?php } ?>
            $("#add_row").click(function () {
                $('#tab_logic').append('<tr id="addr' + (i) + '"></tr>');
                for (var j = 1; j < sum + 1; j++) {
                    $('#addr' + i).append("<td><select name='param" + (j) + "[]' class='form-control'>\
                                                    <option value='0'>" + (opt[0]) + "</option>\
                                                    <option value='1'>" + (opt[1]) + "</option>\
                                                    <option value='2'>" + (opt[2]) + "</option>\
                                                </select>\
                                            </td>");
                }
                $('#addr' + i).append("<td><select name='result[]' class='form-control'>\
                                                    <option value='0'>" + (opt[0]) + "</option>\
                                                    <option value='1'>" + (opt[1]) + "</option>\
                                                    <option value='2'>" + (opt[2]) + "</option>\
                                                </select>\
                                            </td>");
                i++;
            });
            $("#delete_row").click(function () {
                if (i > 0) {
                    $("#addr" + (i - 1)).html('');
                    i--;
                }
            });

        });
    </script>
@endsection

<!-- <tr><td><input type="text" name="field_name[]" value=""/></td><td><input type="text" name="param_value[]" value=""/></td><td><input type="text" name="l_value[]" value=""/></td><td><input type="text" name="m_value[]" value=""/></td><td><input type="text" name="k_value[]" value=""/></td> -->

