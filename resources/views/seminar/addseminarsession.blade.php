@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Tambah Sesi Seminar</div>

                    <div class="panel-body">
                        <form class="form-horizontal" id="addSession" method="POST" action="/addsession/submit">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label class="col-md-2 control-label">NIM Mahasiswa</label>
                                <div class="col-md-6">
                                    <select name="mahasiswa" class="form-control">
                                        <!-- TODO Only listed Mahasiswa in Jadwal and Have Dosen Pembimbing
                                            Should Shown in The List Not All Mahasiswa Shown
                                        -->
                                        <option disabled selected>Pilih Mahasiswa</option>
                                        @foreach ($listEntity as $entity)
                                            <option value="{{$entity->ID}}">{{$entity->ID}} | {{$entity->Nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">NIP Penguji 1</label>
                                <div class="col-md-6">
                                    <select name="dosen2" class="form-control">
                                        <option disabled selected>Pilih Dosen</option>
                                        @foreach ($listDosen as $entity)
                                            <option value="{{$entity->ID}}">{{$entity->ID}} | {{$entity->Nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">NIP Penguji 2</label>
                                <div class="col-md-6">
                                    <!-- TODO Sometimes Update the list without the selected NIP Penguji 1-->
                                    <select name="dosen3" class="form-control">
                                        <option disabled selected>Pilih Dosen</option>
                                        @foreach ($listDosen as $entity)
                                            <option value="{{$entity->ID}}">{{$entity->ID}} | {{$entity->Nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Tipe Seminar</label>
                                <div class="col-md-6">
                                    <select name="tipe" class="form-control">
                                        <option value="0" selected>Seminar Topik</option>
                                        <option value="1">Seminar</option>
                                        <option value="2">Sidang</option>
                                    </select>
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary" value="Create Session">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
