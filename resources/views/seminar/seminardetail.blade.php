@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Detail Seminar</div>

                    <div class="panel-body">
                        <div class="field_wrapper">
                            <form method="POST" action="/seminardetail/update">
                                {!! csrf_field() !!}
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <th>No.</th>
                                        <th>Parameter</th>
                                        <th>NIP Dosen</th>
                                        <th>Nilai</th>
                                        </thead>
                                        <tbody>
                                        @if (!empty($param))
                                            <tr>
                                                <td>-</td>
                                                <td>{!! $param->NamaParameter !!}</td>
                                                <td>-</td>
                                                <td class='col-md-1'>
                                                    <input name='nilai1[]' type='text' class='form-control input-md'
                                                           value="{!!$info->Nilai!!}">
                                                </td>
                                            <tr>
                                            <tr>
                                                <td>Parameter Pembimbing</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            <tr>
                                            @foreach ($param1 as $i => $out)
                                                <tr>
                                                    <?php $num = explode("-", $out->ParameterID); ?>
                                                    <td>
                                                        @foreach ($num as $n)
                                                            {!! $n !!}.
                                                        @endforeach
                                                    </td>
                                                    <td>{!! $out->NamaParameter !!}</td>
                                                    <td>{!! $nilai1[$i]->DosenID !!}</td>
                                                    <td class='col-md-1'>
                                                        @if ($nilai1[$i]->ParamValue === 0)
                                                            <input name='nilai1[]' type='text'
                                                                   class='form-control input-md'
                                                                   value='L'/>
                                                        @elseif ($nilai1[$i]->ParamValue === 1)
                                                            <input name='nilai1[]' type='text'
                                                                   class='form-control input-md'
                                                                   value='M'/>
                                                        @elseif ($nilai1[$i]->ParamValue === 2)
                                                            <input name='nilai1[]' type='text'
                                                                   class='form-control input-md'
                                                                   value='K'/>
                                                        @endif
                                                    </td>
                                                <tr>
                                            @endforeach
                                            <tr>
                                                <td>Parameter Penguji</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            <tr>
                                            @foreach ($param2 as $i => $out)
                                                @if ($out->ParameterID == '-1')
                                                    <tr>
                                                        <td>{!! $out->ParameterID !!}</td>
                                                        <td>{!! $out->NamaParameter !!}</td>
                                                        <td>-</td>
                                                        <td class='col-md-1'>
                                                            @if ($nilai2c->ParamValue === 0)
                                                                <input name='nilai2[]' type='text'
                                                                       class='form-control input-md' value='L'/>
                                                            @elseif ($nilai2c->ParamValue === 1)
                                                                <input name='nilai2[]' type='text'
                                                                       class='form-control input-md' value='M'/>
                                                            @elseif ($nilai2c->ParamValue === 2)
                                                                <input name='nilai2[]' type='text'
                                                                       class='form-control input-md' value='K'/>
                                                            @endif
                                                        </td>
                                                    <tr>
                                                @else
                                                    <tr>
                                                        <?php $num = explode("-", $out->ParameterID); ?>
                                                        <td>
                                                            @foreach ($num as $n)
                                                                {!! $n !!}.
                                                            @endforeach
                                                        </td>
                                                        <td>{!! $out->NamaParameter !!}</td>
                                                        <td>{!! $nilai2a[$i-1]->DosenID !!}</td>
                                                        <td class='col-md-1'>
                                                            @if ($nilai2a[$i-1]->ParamValue === 0)
                                                                <input name='nilai2[]' type='text'
                                                                       class='form-control input-md' value='L'/>
                                                            @elseif ($nilai2a[$i-1]->ParamValue === 1)
                                                                <input name='nilai2[]' type='text'
                                                                       class='form-control input-md' value='M'/>
                                                            @elseif ($nilai2a[$i-1]->ParamValue === 2)
                                                                <input name='nilai2[]' type='text'
                                                                       class='form-control input-md' value='K'/>
                                                            @endif
                                                        </td>
                                                    <tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td>{!! $nilai2b[$i-1]->DosenID !!}</td>
                                                        <td class='col-md-1'>
                                                            @if ($nilai2b[$i-1]->ParamValue === 0)
                                                                <input name='nilai2[]' type='text'
                                                                       class='form-control input-md' value='L'/>
                                                            @elseif ($nilai2b[$i-1]->ParamValue === 1)
                                                                <input name='nilai2[]' type='text'
                                                                       class='form-control input-md' value='M'/>
                                                            @elseif ($nilai2b[$i-1]->ParamValue === 2)
                                                                <input name='nilai2[]' type='text'
                                                                       class='form-control input-md' value='K'/>
                                                            @endif
                                                        </td>
                                                    <tr>
                                                @endif
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <input type="hidden" id="seminar_id" name="seminar_id" value="{!! $id !!}">
                                <input type="submit" class="btn btn-primary" value="Simpan Nilai Parameter">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
