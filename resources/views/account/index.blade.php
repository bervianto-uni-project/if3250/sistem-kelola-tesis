@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @if(session()->has('success'))
                    <div class="alert alert-success">
                        <strong>{{session('success')}}</strong>
                    </div>
                @endif

                @if(session()->has('error'))
                    <div class="alert alert-warning">
                        @foreach(session('error') as $item)
                            <p>Akun dengan id : <strong>{{$item->username}}</strong> sudah ada.</p>
                        @endforeach
                    </div>
                @endif

                @if(session()->has('errorText'))
                    <div class="alert alert-warning">
                        <strong>{{session('errorText')}}</strong>
                    </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">Manajemen Akun</div>

                    <div class="panel-body">
                        <div class="col-md-6">
                            <div id="mahasiswa" class="content">
                                <h2>Tambah Akun Mahasiswa</h2>
                                Format File (.csv):
                                <pre>No;NIM;Nama</pre>
                                Contoh:
                                <pre>1;23514002;Adam Asshidiq</pre>
                                Contoh file : <a href="{{asset('assets/IF6099-01.csv')}}">Download</a>
                                <form method="post" enctype="multipart/form-data" action="/add-account/mahasiswa">
                                    {{csrf_field()}}
                                    <input name="mahasiswa" type="file" accept=".csv, text/csv">
                                    <input name="mahasiswa_input" class="btn btn-primary" type="submit"
                                           value="Upload">
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div id="dosen" class="content">
                                <h2>Tambah Akun Dosen</h2>
                                Format File (.csv):
                                <pre>No;NIP;Nama</pre>
                                Contoh:
                                <pre>1;195407161980111001;Benhard Sitohang</pre>
                                Contoh file : <a href="{{asset('assets/IF6099-01-Dosen.csv')}}">Download</a>
                                <form method="post" enctype="multipart/form-data" action="/add-account/dosen">
                                    {{csrf_field()}}
                                    <input name="dosen" type="file" accept=".csv, text/csv">
                                    <input name="dosen_input" class="btn btn-primary" type="submit" value="Upload">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
