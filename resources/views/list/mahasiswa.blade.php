@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Profil
                        <div style="float: right;">
                            <a href="/bimbingan/mahasiswa/{{ $mahasiswaDetail->NIM }}">
                                <button type="button" class="btn btn-primary btn-sm">Daftar Bimbingan</button>
                            </a>
                        </div>
                    </div>

                    <div class="panel-body">
                        <p>Nama : <b style="color:green;">{{ $mahasiswaDetail->Nama }}</b></p>
                        <p> NIM : <b style="color:green;">{{ $mahasiswaDetail->NIM }}</b></p>
                        @if ($mahasiswaDetail->Approved == 1)
                            <p> Topik : <b style="color:green;">{{ $mahasiswaDetail->Topik }}</b></p>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
