@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Profile</div>

                    <div class="panel-body">
                        <b>
                            {{ $dosenDetail->Nama }} :
                            {{ $dosenDetail->NIP }}
                        </b>
                        <br/>
                        <b>Email : </b> {{ $dosenDetail->Email }} <br/>
                        <b>Telepon : </b> {{ $dosenDetail->Telepon }} <br/>
                        <br/>


                        @if( !$listMhs->isEmpty() )
                            <div class="table-responsive">
                                <table ui-jq="dataTable" class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th>NIM</th>
                                        <th>Nama</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($listMhs as $mahasiswa)
                                        <tr>
                                            <td class="text-center">{{ $loop->iteration }}</td>
                                            <td><a href="mahasiswa/{{ $mahasiswa->ID }}"> {{ $mahasiswa->ID }} </a></td>
                                            <td> {{ $mahasiswa->Nama }} </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            Tidak ada mahasiswa bimbingan
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
