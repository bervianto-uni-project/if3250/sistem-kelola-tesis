@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if ($type == 'mahasiswa')
                            Daftar Mahasiswa
                        @else
                            Daftar Dosen
                        @endif

                    </div>
                    <div class="panel-body">

                        @if( !$listEntity->isEmpty() )
                            <div class="table-responsive">
                                <table ui-jq="dataTable" class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        @if ($type == 'mahasiswa')
                                            <th>NIM</th>
                                        @else
                                            <th>NIP</th>
                                        @endif
                                        <th>Nama</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($listEntity as $entity)
                                        <tr>
                                            <td class="text-center">{{ $loop->iteration }}</td>
                                            @if ($type == 'mahasiswa')
                                                <td><a href="mahasiswa/{{ $entity->ID }}"> {{ $entity->ID }} </a></td>
                                            @else
                                                <td><a href="dosen/{{ $entity->ID }}"> {{ $entity->ID }} </a></td>
                                            @endif
                                            <td> {{ $entity->Nama }} </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            Tidak ada mahasiswa bimbingan
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection