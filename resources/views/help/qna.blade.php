@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Question & Answer</div>
                    <div class="panel-body">
                        <div class="row">
                        <ul>
                            <li>Bagaimana memanfaatkan fitur date picker dan time picker di Firefox?</li>
                            <ol>
                                <li>Masukan url atau link berikut ke kotak alamat browser lalu enter. <pre>about:config</pre></li>
                                <li>Jika ada gambar seperti di bawah ini. Klik <code>I accept the risk!</code> dan centang <code>Show this warning next time</code>.
                                    <img class="img-thumbnail img-responsive" src="{{asset('images/help-firefox-date.PNG')}}"></li>
                                <li>Cari <code>dom.forms.datetime</code>.</li>
                                <li>Jadikan nilai <code>dom.forms.datetime</code> menjadi <code>true</code>. Dapat dengan <code>double click</code> pada bagian tersebut.</li>
                                <li>Pastikan menjadi seperti gambar di bawah ini. <img class="img-thumbnail img-responsive" src="{{asset('images/help-firefox-date-true.PNG')}}"></li>
                            </ol>

                        </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection