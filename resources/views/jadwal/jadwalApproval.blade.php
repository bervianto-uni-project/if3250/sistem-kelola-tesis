@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if( ! empty($errorText))
                    <div class="alert alert-danger">
                        <strong>Insert failed!</strong> {{ $errorText }}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Persetujuan Jadwal
                        <div style="float: right;">
                            <a href="/viewPengajuan">
                                <button type="button" class="btn btn-primary btn-sm">Kembali</button>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/jadwal/confirmApproval">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="ruangan" class="col-md-4 control-label">Ruangan</label>
                                <div class="col-md-6">
                                    <input id="ruangan" type="text" class="form-control" name="ruangan" required
                                           autofocus/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dosen2" class="col-md-4 control-label">NIP Dosen Penguji 1</label>
                                <div class="col-md-6">
                                    <select name="dosen2" class="form-control">
                                        <!--  TODO Automatic Update after Selection Mahasiswa -->
                                        <option disabled selected>Pilih Dosen</option>
                                        @foreach ($listDosen as $entity)
                                            <option value="{{$entity->ID}}">{{$entity->ID}} | {{$entity->Nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dosen3" class="col-md-4 control-label">NIP Dosen Penguji 2</label>
                                <div class="col-md-6">
                                    <select name="dosen3" class="form-control">
                                        <!--  TODO Automatic Update after Selection Mahasiswa -->
                                        <option disabled selected>Pilih Dosen</option>
                                        @foreach ($listDosen as $entity)
                                            <option value="{{$entity->ID}}">{{$entity->ID}} | {{$entity->Nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="pjID" value="{{ $pj->ID }}">
                                <input type="hidden" name="tanggal" value="{{ $pj->Tanggal }}">
                                <input type="hidden" name="waktu_m" value="{{ $pj->Waktu_Awal }}">
                                <input type="hidden" name="waktu_a" value="{{ $pj->Waktu_Akhir }}">
                                <input type="hidden" name="mahasiswaID" value="{{ $pj->MahasiswaID }}">
                                <input type="hidden" name="jenis" value="{{ $pj->Jenis }}">
                                <input type="hidden" name="keterangan" value="{{ $pj->Keterangan }}">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection