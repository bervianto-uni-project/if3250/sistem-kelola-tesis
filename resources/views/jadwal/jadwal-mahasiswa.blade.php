@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
                    <div class="panel-body">
                        <div class="center">
                            <ul class="nav nav-pills">
                                <li class="active"><a data-toggle="pill" href="#jadwal">
                                        @if(Auth::user()->role == "Mahasiswa")
                                            Jadwal Keseluruhan
                                        @endif
                                    </a></li>
                                <li><a data-toggle="pill" href="#jadwalMhs">
                                        @if(Auth::user()->role == "Mahasiswa")
                                            Jadwal Saya
                                        @endif
                                    </a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="center">
                            <form class="form-inline">
                                <div class="col-md-4 text-center">
                                    <div class="form-group">
                                        <label for="bulan">Bulan</label>
                                        <select class="form-control" id="bulan" name="bulan">
                                            <option value="1">Januari</option>
                                            <option value="2">Februari</option>
                                            <option value="3">Maret</option>
                                            <option value="4">April</option>
                                            <option value="5">Mei</option>
                                            <option value="6">Juni</option>
                                            <option value="7">Juli</option>
                                            <option value="8">Agustus</option>
                                            <option value="9">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 text-center">
                                    <div class="form-group">
                                        <label for="tahun">Tahun</label>
                                        <select class="form-control" id="tahun" name="tahun">
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 text-center">
                                    <a href="/jadwal/jadwalSpec">
                                        <button type="submit" class="btn btn-primary btn-block">Cari</button>
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="jadwal" class="tab-pane fade in active">
                                @if ( !$jadwals->isEmpty() )
                                    <table class="table table-hover table-responsive">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tanggal</th>
                                            <th>Waktu</th>
                                            <th>Kegiatan</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($jadwals as $j)
                                            <tr>
                                                <td>{{ $j->ID }}</td>
                                                <td>{{ $j->Tanggal }}</td>
                                                <td>{{ $j->Waktu_Mulai }} - {{ $j->Waktu_Akhir }}</td>
                                                <td>{{ $j->Kegiatan }}</td>
                                                @if(Auth::user()->role="Mahasiswa")
                                                    <td><a href="jadwal/ajukan/{{ $j->ID }}">
                                                            <button class="btn btn-default btn-block">Ajukan</button>
                                                        </a></td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <p>Belum ada jadwal</p>
                                @endif
                            </div>

                            <div id="jadwalMhs" class="tab-pane fade">
                                @if ( !$pjadwals->isEmpty() )
                                    <table class="table table-hover table-responsive">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Mahasiswa</th>
                                            <th>Tanggal</th>
                                            <th>Waktu</th>
                                            <th>Kegiatan</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($pjadwals as $pj)
                                            <tr>
                                                <td>{{ $pj->ID }}</td>
                                                <td>{{ $pj->Nama }}</td>
                                                <td>{{ $pj->Tanggal }}</td>
                                                <td>{{ $pj->Waktu }}</td>
                                                <td>{{ $pj->Kegiatan }}</td>
                                                @if(Auth::user()->role="Mahasiswa")
                                                    @if ($pj->Approved == '0')
                                                        <td>Not Approved</td>
                                                    @else
                                                        <td>Approved</td>
                                                    @endif
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <p>Belum ada jadwal</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection