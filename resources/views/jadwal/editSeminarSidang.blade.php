@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if( ! empty($errorText))
                    <div class="alert alert-danger">
                        <strong>Insert failed!</strong> {{ $errorText }}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Jadwal Seminar/Sidang
                        <div style="float: right;">
                            <a href="/jadwal">
                                <button type="button" class="btn btn-primary btn-sm">Kembali</button>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="GET" action="/jadwal/seminarsidang">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="nama" class="col-md-4 control-label">Nama Mahasiswa</label>
                                <div class="col-md-6">
                                    <input type="text" id="nama" class="form-control" name="nama"
                                           value="<?php echo $sj->Nama; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tanggal" class="col-md-4 control-label">Tanggal</label>
                                <div class="col-md-6">
                                    <input id="tanggal" type="text" class="form-control" name="tanggal"
                                           value="<?php echo $sj->Tanggal; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="waktu_aw" class="col-md-4 control-label">Waktu Awal</label>
                                <div class="col-md-6">
                                    <input id="waktu_aw" type="text" class="form-control" name="waktu_aw"
                                           value="<?php echo $sj->Waktu_Awal; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="waktu_a" class="col-md-4 control-label">Waktu Akhir</label>
                                <div class="col-md-6">
                                    <input id="waktu_a" type="text" class="form-control" name="waktu_a"
                                           value="<?php echo $sj->Waktu_Akhir; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="jenis">Ruangan</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="ruangan" name="ruangan"
                                           value="<?php echo $sj->Ruangan; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="jenis">Jenis Kegiatan</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="jenis" name="jenis"
                                           value="<?php echo $sj->Tipe; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="keterangan" class="col-md-4 control-label">Keterangan</label>
                                <div class="col-md-6">
                                    <input type="text" id="keterangan" class="form-control" name="keterangan"
                                           value="<?php echo $sj->Keterangan; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="dosenA">NIP Dosen Pembimbing:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="dosenA" name="dosenA"
                                           value="<?php echo $sj->DosenAID; ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="dosenA">NIP Dosen Penguji 1:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="dosenB" name="dosenB"
                                           value="<?php echo $sj->DosenBID; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="dosenC">NIP Dosen Penguji 2:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="dosenC" name="dosenC"
                                           value="<?php echo $sj->DosenCID; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="idSeminar" value="{{ $sj->ID }}"/>
                                <input type="hidden" name="dosbing" value="{{ $sj->DosenAID }}"/>
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection