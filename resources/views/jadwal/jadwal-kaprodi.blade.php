@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
                    <br><br>

                    <div>
                        <form class="form-inline">
                            <div class="col-md-4 text-center">
                                <div class="form-group">
                                    <label for="bulan">Bulan</label>
                                    <select class="form-control" id="bulan" name="bulan">
                                        <option value="1">Januari</option>
                                        <option value="2">Februari</option>
                                        <option value="3">Maret</option>
                                        <option value="4">April</option>
                                        <option value="5">Mei</option>
                                        <option value="6">Juni</option>
                                        <option value="7">Juli</option>
                                        <option value="8">Agustus</option>
                                        <option value="9">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 text-center">
                                <div class="form-group">
                                    <label for="tahun">Tahun</label>
                                    <select class="form-control" id="tahun" name="tahun">
                                        <option value="2017">2017</option>
                                        <option value="2018">2018</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 text-center">
                                <a href="/jadwal/jadwalSpec">
                                    <button type="submit" class="btn btn-primary btn-block">Cari</button>
                                </a>
                            </div>
                        </form>
                    </div>
                    <br> <br> <br>

                    <div>

                    </div>
                    <br><br>
                </div>
            </div>
        </div>
    </div>
@endsection