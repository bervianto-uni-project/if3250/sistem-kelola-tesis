@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <!-- Header Halaman -->
                    <div class="panel-heading">Daftar Kegiatan/Jadwal</div>

                    <!-- Bagian Pills -->
                    @if(Auth::user()->role != "Kaprodi")
                        <div class="panel-body">
                            <div class="center">
                                <ul class="nav nav-pills">
                                    <li class="active"><a data-toggle="pill" href="#jadwal">
                                            Jadwal Keseluruhan
                                        </a></li>
                                    @if(Auth::user()->role!="Dosen")
                                        <li><a data-toggle="pill" href="#jadwalMhs">
                                                Permintaan Jadwal
                                            </a></li>
                                    @endif
                                    <li><a data-toggle="pill" href="#jadwalMhsSpek">
                                            Jadwal Spesifik
                                        </a></li>
                                </ul>
                            </div>
                        </div>
                @endif

                <!-- Tabel Viewer -->
                    <div class="panel-body">
                        <!-- Role Selain Kaprodi -->
                        @if(Auth::user()->role != "Kaprodi")
                            <div class="tab-content">

                                <!-- Bagian jadwal keseluruhan -->
                                <div id="jadwal" class="tab-pane fade in active">
                                    @if ( !$jadwals->isEmpty() )
                                        <div class="table-responsive">
                                            <table ui-jq="dataTable" class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Waktu Mulai</th>
                                                    <th>Waktu Akhir</th>
                                                    <th>Kegiatan</th>
                                                    <th>Keterangan</th>
                                                    @if((Auth::user()->role=="Mahasiswa")||(Auth::user()->role == "Admin"))
                                                        <th></th>
                                                    @endif
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($jadwals as $j)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ $j->Waktu_Mulai }}</td>
                                                        <td> {{ $j->Waktu_Akhir }}</td>
                                                        <td>{{ $j->Jenis_Kegiatan }}</td>
                                                        <td>{{ $j->Keterangan }}</td>
                                                        @if(Auth::user()->role=="Mahasiswa")
                                                            <td><a href="jadwal/ajukan/{{ $j->ID }}">
                                                                    <button class="btn btn-default btn-block">Ajukan
                                                                    </button>
                                                                </a></td>
                                                        @elseif(Auth::user()->role == "Admin")
                                                            <td><a href="/jadwal/edit/{{ $j->ID }}">
                                                                    <button class="btn btn-default btn-block">Ubah
                                                                    </button>
                                                                </a></td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    @else
                                        <p>Belum ada jadwal</p>
                                    @endif

                                    @if(Auth::user()->role=="Admin")
                                        <a href="/jadwal/jadwalBaru">
                                            <button class="btn btn-primary btn-block">Tambah Jadwal</button>
                                        </a>
                                    @endif
                                </div>

                            @if(Auth::user()->role!="Dosen")
                                <!-- Permintaan Jadwal -->
                                    <div id="jadwalMhs" class="tab-pane fade">
                                        @if ( !$pjadwals->isEmpty() )
                                            <div class="table-responsive">
                                                <table ui-jq="dataTable" class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        @if(Auth::user()->role!="Mahasiswa")
                                                            <th>Mahasiswa</th>
                                                        @endif
                                                        <th>Tanggal</th>
                                                        @if(Auth::user()->role != "Admin")
                                                            <th>Waktu Mulai</th>
                                                            <th>Waktu Akhir</th>
                                                        @endif
                                                        <th>Kegiatan</th>
                                                        <th>Keterangan</th>
                                                        @if(Auth::user()->role == "Mahasiswa")
                                                            <th>Status</th>
                                                        @else
                                                            <th></th>
                                                        @endif
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($pjadwals as $pj)
                                                        <tr>
                                                            <td>{{ $loop->iteration }}</td>
                                                            @if(Auth::user()->role != "Mahasiswa")
                                                                <td>{{ $pj->Nama }}</td>
                                                            @endif
                                                            <td>{{ $pj->Tanggal }}</td>
                                                            @if(Auth::user()->role != "Admin")
                                                                <td>{{ $pj->Waktu_Awal }}</td>
                                                                <td>{{ $pj->Waktu_Akhir }}</td>
                                                            @endif
                                                            <td>{{ $pj->Jenis_Kegiatan }}</td>
                                                            <td>{{ $pj->Keterangan }}</td>
                                                            <!-- Kolom Terakhir disesuaikan dengan Role -->

                                                            @if(Auth::user()->role!="Admin")
                                                                @if ($pj->Approved == '0')
                                                                    <td>Belum Disetujui</td>
                                                                @elseif ($pj->Approved == '-1')
                                                                    <td>Ditolak</td>
                                                                @else
                                                                    <td>Disetujui</td>
                                                                @endif
                                                            @elseif(Auth::user()->role=="Admin")
                                                                @if ($pj->Approved == '0')
                                                                    <td>
                                                                        <form action="/jadwal/view" method="GET">
                                                                            {{ csrf_field() }}
                                                                            <input type="hidden" name="pjID"
                                                                                   value="{{ $pj->ID }}">
                                                                            <input type="submit"
                                                                                   class="btn btn-primary btn-block"
                                                                                   value="View">
                                                                        </form>
                                                                    </td>
                                                                @else
                                                                    <td>
                                                                        <button class="btn btn-default btn-block"
                                                                                disabled="true">Lihat
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        @else
                                            <p>Belum ada jadwal</p>
                                        @endif
                                    </div>
                                @endif

                                @if(Auth::user()->role=="Mahasiswa")
                                    <div id="jadwalMhsSpek" class="tab-pane fade">
                                        @if ( !$spekjadwals->isEmpty() )
                                            <div class="table-responsive">
                                                <table ui-jq="dataTable" class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Tanggal</th>
                                                        <th>Waktu</th>
                                                        <th>Ruangan</th>
                                                        <th>Dosen Pembimbing</th>
                                                        <th>Dosen Penguji</th>
                                                        <th>Keterangan</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($spekjadwals as $sj)
                                                        <tr>
                                                            <td>{{ $loop->iteration }}</td>
                                                            <td>{{ $sj->Tanggal }}</td>
                                                            <td>{{ $sj->Waktu_Awal }} - {{ $sj->Waktu_Akhir }}</td>
                                                            <td>{{ $sj->Ruangan }}</td>
                                                            <td>{{ $sj->DosenA }}</td>
                                                            <td>{{ $sj->DosenB }} dan <br>
                                                                {{ $sj->DosenC }}</td>
                                                            <td>{{ $sj->Keterangan }}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        @else
                                            <p>Belum ada jadwal</p>
                                        @endif
                                    </div>
                                @elseif (Auth::user()->role=="Dosen")
                                    <div id="jadwalMhsSpek" class="tab-pane fade">
                                        @if ((!$spekjadwalsA->isEmpty())||(!$spekjadwalsB->isEmpty()))
                                            <div class="table-responsive">
                                                <table ui-jq="dataTable" class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Tanggal</th>
                                                        <th>Waktu</th>
                                                        <th>Ruangan</th>
                                                        <th>Mahasiswa</th>
                                                        <th>Peran</th>
                                                        <th>Keterangan</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if (!$spekjadwalsA->isEmpty())
                                                        @foreach ($spekjadwalsA as $sjA)
                                                            <tr>
                                                                <td>{{ $loop->iteration }}</td>
                                                                <td>{{ $sjA->Tanggal }}</td>
                                                                <td>{{ $sjA->Waktu_Awal }}
                                                                    - {{ $sjA->Waktu_Akhir }}</td>
                                                                <td>{{ $sjA->Ruangan }}</td>
                                                                <td>{{ $sjA->Nama }}</td>
                                                                <td>Pembimbing</td>
                                                                <td>{{ $sjA->Keterangan }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    @if (!$spekjadwalsB->isEmpty())
                                                        @foreach ($spekjadwalsB as $sjB)
                                                            <tr>
                                                                <td>{{ $loop->iteration }}</td>
                                                                <td>{{ $sjB->Tanggal }}</td>
                                                                <td>{{ $sjB->Waktu_Awal }}
                                                                    - {{ $sjB->Waktu_Akhir }}</td>
                                                                <td>{{ $sjB->Ruangan }}</td>
                                                                <td>{{ $sjB->Nama }}</td>
                                                                <td>Penguji</td>
                                                                <td>{{ $sjB->Keterangan }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        @else
                                            <p>Belum ada jadwal</p>
                                        @endif
                                    </div>
                                @else
                                    <div id="jadwalMhsSpek" class="tab-pane fade">
                                        @if (!$spekjadwals->isEmpty())
                                            <div class="table-responsive">
                                                <table ui-jq="dataTable" class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Tanggal</th>
                                                        <th>Waktu</th>
                                                        <th>Ruangan</th>
                                                        <th>Mahasiswa</th>
                                                        <th>Keterangan</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($spekjadwals as $sj)
                                                        <tr>
                                                            <td>{{ $loop->iteration }}</td>
                                                            <td>{{ $sj->Tanggal }}</td>
                                                            <td>{{ $sj->Waktu_Awal }} - {{ $sj->Waktu_Akhir }}</td>
                                                            <td>{{ $sj->Ruangan }}</td>
                                                            <td>{{ $sj->Nama }}</td>
                                                            <td>{{ $sj->Keterangan }}</td>
                                                            <td>
                                                                <form action="/jadwal/seminarsidang/edit/{{ $sj->ID }}"
                                                                      method="GET">
                                                                    <input type="submit"
                                                                           class="btn btn-primary btn-block"
                                                                           value="Edit">
                                                                </form>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        @else
                                            <p>Belum ada jadwal</p>
                                        @endif
                                    </div>
                                @endif
                            </div>
                        @else
                            @if ( !$jadwals->isEmpty())
                                <div class="table-responsive">
                                    <table ui-jq="dataTable" class="table table-hover table-responsive">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Waktu_Mulai</th>
                                            <th>Waktu_Akhir</th>
                                            <th>Kegiatan</th>
                                            <th>Keterangan</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($jadwals as $j)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $j->Waktu_Mulai }}</td>
                                                <td>{{ $j->Waktu_Akhir }}</td>
                                                <td>{{ $j->Jenis_Kegiatan }}</td>
                                                <td>{{ $j->Keterangan }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                <p> Belum ada jadwal </p>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection