@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Profile : {{ $user->name }}</div>

                    <div class="panel-body">
                        <div class="content">
                            <img id="user-photo" src="{{asset('images/avatar-default.jpg')}}" alt="avatar_default" style="width: 100px;height: 100px" class="img-circle">
                            <p>Nama : {{$user->name}}</p>
                            <p>Email : {{$user->email}}</p>
                            <p>Username : {{$user->username}}</p>
                            <form action="/edit-profile">
                                <input class="btn btn-warning" type="submit" value="Edit Profile">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection