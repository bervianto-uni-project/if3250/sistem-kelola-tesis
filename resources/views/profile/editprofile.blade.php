@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Profile | {{ $user->name }}</div>

                    <div class="panel-body">

                        <form class="form-horizontal" style="padding: 10px" role="form" method="post" action="/update-profile">
                            {{ csrf_field() }}
                            <label for="name" class="control-label">Nama : </label>
                            <input type="text" style="margin: 10px" class="form-control" name="name" id="name" value="{{$user->name}}" disabled>
                            <label for="username">Username : </label>
                            <input type="text" style="margin: 10px" class="form-control" name="username" id="username" value="{{$user->username}}" disabled>
                            <label for="email">E-mail : </label>
                            <input type="email" style="margin: 10px" class="form-control" name="email" id="email" value="{{$user->email}}">
                            <label for="password">Password : </label>
                            <input type="password" style="margin: 10px" class="form-control" name="password" id="password">
                            <label for="confirm_password">Confirm Password : </label>
                            <input type="password" style="margin: 10px" class="form-control" name="confirm_password" id="confirm_password">
                            <input type="submit" class="btn btn-success" value="Update">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection