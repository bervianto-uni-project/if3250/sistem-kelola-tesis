@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Bimbingan</div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <?php $idx = 0; ?>
                            @if( !$bimbingans->isEmpty() )
                                <div class="table-responsive">
                                    <table ui-jq="dataTable" class="table table-hover table-responsive">
                                        <thead>
                                        <tr>
                                            <th>NIM</th>
                                            <th>Nama</th>
                                            <th>Tanggal</th>
                                            <th>Catatan</th>
                                            <th>Approved</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($bimbingans as $bimbingan)
                                            <tr>

                                                <td>
                                                    <a href="/mahasiswa/{{ $bimbingan->MahasiswaID }}"> {{ $bimbingan->MahasiswaID }} </a>
                                                </td>
                                                <td> {{ $bimbingan->Nama }} </td>
                                                <td><a href="/bimbingan/{{ $bimbingan->ID }}">
                                                        {{ date("d-m-Y", strtotime($bimbingan->Tanggal)) }}
                                                    </a></td>
                                                <td>
                                                    <div class="limit-text-small"> {{ $bimbingan->Catatan }} </div>
                                                </td>
                                                <td>
                                                    @for ($idx = 0; $idx < count($bimbingan->Approved); $idx++)
                                                        @if ($bimbingan->Approved[$idx] == 0)
                                                            <div class="bg-danger text-danger text-center">
                                                                {{ $bimbingan->Dosen[$idx] }}
                                                            </div>
                                                        @else
                                                            <div class="bg-success text-success text-center">
                                                                {{ $bimbingan->Dosen[$idx] }}
                                                            </div>
                                                        @endif
                                                    @endfor

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                Belum ada bimbingan yang dilakukan
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
