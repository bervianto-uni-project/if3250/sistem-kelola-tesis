@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Bimbingan
                        @if( Auth::user()->role == 'Mahasiswa')
                            <div class="pull-right">
                                <a href="bimbingan/tulis">
                                    <button type="button" class="btn btn-primary btn-sm">Catat Bimbingan Baru</button>
                                </a>
                            </div>
                        @endif
                    </div>
                    <div class="panel-body">

                        <?php $idx = 0; ?>
                        @if( !$bimbingans->isEmpty() )
                            <div class="table-responsive">
                                <table ui-jq="dataTable" class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Catatan</th>
                                        <th>Rencana</th>
                                        @if (Auth::user()->role == 'Mahasiswa')
                                            <th>Persetujuan</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($bimbingans as $bimbingan)
                                        <tr>
                                            <td> {{ $loop->iteration }} </td>
                                            <td><a href="/bimbingan/{{ $bimbingan->ID }}">
                                                    {{ date("d-m-Y", strtotime($bimbingan->Tanggal)) }}
                                                </a></td>
                                            <td>
                                                <div class="limit-text-small"> {{ $bimbingan->Catatan }} </div>
                                            </td>
                                            <td>
                                                <div class="limit-text-small"> {{ $bimbingan->Rencana }} </div>
                                            </td>
                                            @if (Auth::user()->role == 'Mahasiswa')
                                                <td>
                                                    @if (count($bimbingan->Approved) > 0)
                                                        @for ($idx = 0; $idx < count($bimbingan->Approved); $idx++)
                                                            <div class="bg-success text-success text-center">
                                                                {{ $bimbingan->Dosen[$idx] }}
                                                            </div>
                                                        @endfor
                                                    @else
                                                        Belum disetujui
                                                    @endif
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            Belum ada bimbingan yang dilakukan
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection