<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create user trigger based on role
        DB::unprepared("
            CREATE TRIGGER `user_trig` AFTER INSERT ON `users` FOR EACH ROW
            IF (NEW.role = 'Mahasiswa') THEN BEGIN INSERT INTO mahasiswa (NIM, Nama, Email, Topik, Approved)
            VALUES (NEW.username, NEW.name, NEW.email, '', false); END;
            ELSEIF (NEW.role = 'Dosen') THEN BEGIN INSERT INTO dosen (NIP, Nama, Email, Telepon)
            VALUES (NEW.username, NEW.name, NEW.email, ''); END; END IF;
            ");

        // Email update
        DB::unprepared("
            CREATE TRIGGER `user_email_trig` AFTER UPDATE ON `users` FOR EACH ROW
            IF (NEW.role = 'Mahasiswa') THEN
                BEGIN
                    UPDATE mahasiswa
                    SET mahasiswa.Email = NEW.email
                    WHERE mahasiswa.NIM = NEW.id;
                END;
            ELSEIF (NEW.role = 'Dosen') THEN
                BEGIN
                    UPDATE dosen
                    SET dosen.Email = NEW.email
                    WHERE dosen.NIP = NEW.id;
                END;
            END IF;
            ");

        //create bimbingan dosen based on bimbingan
        DB::unprepared("
            CREATE TRIGGER `bimbingan_trig` AFTER INSERT ON `bimbingan` FOR EACH ROW

            INSERT INTO bimbingan_dosbing
            SELECT NEW.ID, DosenID, false
            FROM mahasiswa_dosbing
            WHERE mahasiswa_dosbing.MahasiswaID = NEW.MahasiswaID AND mahasiswa_dosbing.Approved = 1;
            ");

        DB::unprepared("
            CREATE TRIGGER `jadwal_trig` AFTER UPDATE ON `jadwal` FOR EACH ROW
            BEGIN
            UPDATE pengajuan_jadwal
            SET pengajuan_jadwal.Keterangan = NEW.Keterangan,
                pengajuan_jadwal.Jenis_Kegiatan = NEW.Jenis_Kegiatan
            WHERE pengajuan_jadwal.jadwal_id = NEW.ID;
            
            UPDATE seminar_sidang
            SET seminar_sidang.Keterangan = NEW.Keterangan,
                seminar_sidang.Tipe = NEW.Jenis_Kegiatan
            WHERE seminar_sidang.jadwal_id = NEW.ID;
            END;
            ");

        DB::unprepared("
            CREATE TRIGGER `edit_seminar` AFTER UPDATE ON `seminar_sidang` FOR EACH ROW

            UPDATE pengajuan_jadwal
            SET pengajuan_jadwal.Keterangan = NEW.Keterangan,
                pengajuan_jadwal.Jenis_Kegiatan = NEW.Tipe
            WHERE pengajuan_jadwal.ID = NEW.permintaan_jadwal_id;
            ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `user_trig`');
        DB::unprepared('DROP TRIGGER `bimbingan_trig`');
        DB::unprepared('DROP TRIGGER `jadwal_trig`');
        DB::unprepared('DROP TRIGGER `edit_seminar`');
    }
}
