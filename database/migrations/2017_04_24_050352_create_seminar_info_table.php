<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeminarInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seminar_info', function (Blueprint $table) {
            $table->increments('SeminarID');
            $table->text('Catatan_1')->nullable();
            $table->text('Catatan_2')->nullable();;
            $table->text('Catatan_3')->nullable();
            $table->string('Nilai');
            $table->boolean('Active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seminar_info');
    }
}
