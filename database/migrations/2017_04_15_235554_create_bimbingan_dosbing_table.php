<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBimbinganDosbingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bimbingan_dosbing', function (Blueprint $table) {
            $table->integer('BimbinganID');
            $table->string('DosenID');
            $table->boolean('Approved');
            $table->primary(['BimbinganID', 'DosenID']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bimbingan_dosbing');
    }
}
