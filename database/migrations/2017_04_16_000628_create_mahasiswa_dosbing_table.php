<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahasiswaDosbingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa_dosbing', function (Blueprint $table) {
            $table->string('MahasiswaID');
            $table->string('DosenID');
            $table->tinyInteger('Approved');
            $table->primary(['MahasiswaID', 'DosenID']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa_dosbing');
    }
}
