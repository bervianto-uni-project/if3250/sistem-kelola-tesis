<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeminarSidangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seminar_sidang', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('jadwal_id')->unsigned();
            $table->foreign('jadwal_id')->references('ID')->on('jadwal')->onDelete('cascade');
            $table->integer('permintaan_jadwal_id')->unsigned();
            $table->foreign('permintaan_jadwal_id')->references('ID')->on('pengajuan_jadwal')->onDelete('cascade');
            $table->date('Tanggal');
            $table->time('Waktu_Awal');
            $table->time('Waktu_Akhir');
            $table->string('Ruangan');
            $table->string('Keterangan')->nullable();
            $table->string('MahasiswaID');
            $table->string('DosenAID');
            $table->string('DosenBID');
            $table->string('DosenCID');
            $table->index('MahasiswaID');
            $table->tinyInteger('Tipe');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seminar_sidang');
    }
}
