<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\ParameterPenilaian;
use App\Models\ParameterDetail;
use App\Models\ParameterOption;
use DB;

class ParameterController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  
    public function edit(Request $request) {
      if (Auth::user()->role == 'Admin') {
	      $param = ParameterPenilaian::where('Tipe', $request->tipe)->where('ParameterID',$request->param_id)->first();
	      if ($param == null) { //New parameter
	      	if ($request->nama_param == null) {
	      		return $this->load($request->tipe, $request->param_id)
	              ->with('errorText', 'Nama parameter tidak boleh kosong');
	          }
	        $param = new ParameterPenilaian;
	        $param->Tipe = $request->tipe;
	        $param->ChildSum = 0;
	        $level = explode('-',$request->param_id);
	        $param->Level = count($level);

			//Get parent id
	        if (strlen($request->param_id) > 1) {
	          $id = explode('-',$request->param_id);
	          $slice = array_slice($id, 0, -1);
	          $string = implode('-',$slice);
	        } else {
	          if ($request->param_id == '0') {
	          	$string = '-';
	          } else {
	            $string = '0';
	          }
	        }
	        $param->Parent = $string;

	        $param->ParameterID = $request->param_id;
	        $param->NamaParameter = $request->nama_param;
	        $param->Deskripsi_L = $request->deskripsi_L;
	        $param->Deskripsi_M = $request->deskripsi_M;
	        $param->Deskripsi_K = $request->deskripsi_K;
	        $param->save();
	      } else {
	      	if ($request->nama_param != null || $request->nama_param != '') {
	      		$q = 'UPDATE parameter_penilaian SET NamaParameter = ? where Tipe = ? and ParameterID = ?';
		        DB::update($q, [$request->nama_param, $request->tipe, $request->param_id]);
		    }
      		if ($request->deskripsi_M != null || $request->deskripsi_L != '') {
      			$q = 'UPDATE parameter_penilaian SET Deskripsi_L = ? where Tipe = ? and ParameterID = ?';
		        DB::update($q, [$request->deskripsi_L, $request->tipe, $request->param_id]);
		    }
      		if ($request->deskripsi_K != null || $request->deskripsi_M != '') {
      			$q = 'UPDATE parameter_penilaian SET Deskripsi_M = ? where Tipe = ? and ParameterID = ?';
		        DB::update($q, [$request->deskripsi_M, $request->tipe, $request->param_id]);
		    }
      		if ($request->deskripsi_K != null || $request->deskripsi_K != '') {
      			$q = 'UPDATE parameter_penilaian SET Deskripsi_K = ? where Tipe = ? and ParameterID = ?';
		        DB::update($q, [$request->deskripsi_K, $request->tipe, $request->param_id]);	
		    }      	
	      }
	      if ($param->Parent != null) {
	      	  $tipe = $request->tipe;
	      	  if ($request->param_id == '0' || $request->param_id == '-1') {
	      	  	$slice = explode('_',$request->tipe);
	      	  	if (end($slice) == '0' || $request->param_id == '-1') {
	      	  		$slice = array_slice($slice, 0, -1);
	            	$slice = implode('',$slice);
	            	$tipe = $slice;
	      	  	} 
	      	  }
		      //Update childsum
		      $parent = ParameterPenilaian::where('Tipe', $tipe)->where('ParameterID',$param->Parent)->first();
		      //Count child
		      $sum = ParameterPenilaian::where('Tipe', $tipe)->where('Parent',$param->Parent)->count();
		      //Update record
		      $q = 'UPDATE parameter_penilaian SET ChildSum = ? where Tipe = ? and ParameterID = ?';
		      DB::update($q, [$sum, $tipe, $parent->ParameterID]);
		  }

	      if ($request->result != null) {
	      	$detail = ParameterDetail::where('Tipe', $request->tipe)->where('ParameterID',$request->param_id)->delete();
	        $result = $request->result;
	        foreach ($result as $n => $det) {
        	  if ($det == null) {
	          return $this->load($request->tipe, $request->param_id)
	              ->with('errorText', 'Parameter tidak boleh kosong');
	          }
	          $detail = new ParameterDetail;
	          $detail->ParameterID = $request->param_id;
	          $detail->Tipe = $request->tipe;
	          $input = array();
	          $count = $param->ChildSum;
	          for ($i = 1; $i <= $count; $i++) {
	            $input[] = $request->{'param' . $i}[$n];
	          }
	          $kriteria = implode('-',$input);
	          if ($kriteria == null) {
		        return $this->load($request->tipe, $request->param_id)
		        	->with('errorText', 'Parameter tidak boleh kosong');
		      } else {
		      	$check = ParameterDetail::where('Tipe', $request->tipe)
			          ->where('ParameterID',$request->param_id)
			          ->where('Kriteria',$kriteria)
			          ->first();
			      if ($check != null) {
			      	return $this->load($request->tipe, $request->param_id)
		        		->with('errorText', 'Terdapat parameter duplikat pada input');
			      }
		      }
	          
	          $detail->Kriteria = $kriteria;
	          $detail->HasilParameter = $det;
	          $detail->save();
	        }
	      }
        return redirect('/daftarparameter');
      }
      return redirect()->action('HomeController@index');
    }

    public function delete(Request $request) {
      if (Auth::user()->role == 'Admin') {
      	  $current = ParameterPenilaian::where('Tipe',$request->tipe)->where('ParameterID',$request->param_id)->first();
	      $delete1 = ParameterPenilaian::where('Tipe',$request->tipe)->where('ParameterID','like',$request->param_id . '%')->delete();
	      $delete2 = ParameterDetail::where('Tipe',$request->tipe)->where('ParameterID','like',$request->param_id . '%')->delete();

	      //Add parent child count
	      //Get parent id
	      if ($request->param_id != '-') {
		      //Count child
	      	  $sum = ParameterPenilaian::where('Tipe',$request->tipe)->where('Parent',$current->Parent)->count();
	      	  $q = 'UPDATE parameter_penilaian SET ChildSum = ? WHERE Tipe = ? and ParameterID = ?';
		      DB::update($q, [$sum, $request->tipe, $current->Parent]);

	      	  $parent = ParameterDetail::where('Tipe',$request->tipe)->where('ParameterID',$current->Parent)->get();
	      	  foreach ($parent as $p) {
	      	  	$param = explode('-',$p->Kriteria);
	      	  	if (count($param) == 1) {
	      	  		$q = 'DELETE FROM parameter_detail WHERE Tipe = ? and ParameterID = ? and Kriteria = ?';
			      	DB::update($q, [$request->tipe, $current->Parent, $p->Kriteria]);
	      	  	} else if (count($param) > $sum) {
			      	$sliced = array_slice($param, 0, -1);
			      	$newparam = implode('-',$sliced);
			      	$q = 'UPDATE parameter_detail SET Kriteria = ? WHERE Tipe = ? and ParameterID = ? and Kriteria = ?';
			      	DB::update($q, [$newparam, $request->tipe, $current->Parent, $p->Kriteria]);
			      }
	      	  }
		    }
		    //return $current;
	        return redirect('/daftarparameter');
      }
      return redirect()->action('HomeController@index');
    }

    public function load($tipe, $id) {
      if (Auth::user()->role == 'Admin') {
	      $outputs = array();
	      $title = array();
	      $check = false;

	      $option = ParameterOption::all();
	      if ($id == '-' || $id == '-1') {
	      	  $info = ParameterPenilaian::where('Tipe', $tipe)->where('ParameterID',$id)->first();
	      	  $output = ParameterDetail::where('Tipe', $tipe)->where('ParameterID',$id)->get();
	      	  foreach ($output as $k) {
		          $detail = explode('-',$k->Kriteria);
		          $outputs[] = array('Kriteria' => $detail, 'Hasil' => $k->HasilParameter);
		      }
		      $sum = $info->ChildSum;
		      if ($id == '-') {
		      	$child = ParameterPenilaian::where('Tipe', 'like', $tipe . '%')->where('Parent',$id)->get();
			      foreach ($child as $c) {
			        $title[] = $c->NamaParameter;
			      }
		      } else {
		      	$title[] = 'Penguji 1';
		      	$title[] = 'Penguji 2';
		      } 
		      $next = null;
	  	  } else {
	      	  $info = ParameterPenilaian::where('Tipe', $tipe)->where('ParameterID',$id)->first();
		      if ($info != null) {
		        $output = ParameterDetail::where('Tipe', $tipe)->where('ParameterID',$id)->get();
		        foreach ($output as $k) {
		            $detail = explode('-',$k->Kriteria);
		            $outputs[] = array('Kriteria' => $detail, 'Hasil' => $k->HasilParameter);
		        }
		        //$outputs->orderBy('Hasil','desc');
		        $sum = $info->ChildSum;

		        if ($sum > 0) {
		          $child = ParameterPenilaian::where('Tipe', $tipe)->where('Parent',$id)->get();
		          foreach ($child as $c) {
		            $title[] = $c->NamaParameter;
		          }
		        }

		        $num = explode('-',$id);
				if ($id != '0') {
					$parent = ParameterPenilaian::where('Tipe', $tipe)->where('ParameterID',$info->Parent)->first();
					if ($parent->ChildSum == end($num)) {
				    	$check = true;
					}
				}	
		      } else {
		      	$sum = 0;
		      	//$check = true;
		      }
		      $num = explode('-',$id);		      
		      $sliced = array_slice($num, 0, -1);
		      if (strlen($id) == 1) {
		        $string = $id + 1;
		      } else {
		      $sliced[] = end($num) + 1;
		      $string = implode('-',$sliced);
		      }
		      $next = array();
		      $next[] = $string;
		      $next[] = $id . '-1';
		      }

		      if ($id == '1')
		      	$check = false;
	      return view('seminar.editparameter', compact('outputs','info','title','id','next','sum','tipe','check','option'));
	      //return $sum;
      }
      return redirect()->action('HomeController@index');
    }

    public function daftar() {
      if (Auth::user()->role == 'Admin') {
      	  $param1 = ParameterPenilaian::where('Tipe', '1')->first();
      	  $param2 = ParameterPenilaian::where('Tipe', '1')->first();
      	  $param3 = ParameterPenilaian::where('Tipe', '1')->first();
	      $param1a = ParameterPenilaian::where('Tipe', '1_0')->orderBy('ParameterID')->get();
	      $param1b = ParameterPenilaian::where('Tipe', '1_1')->orderBy('ParameterID')->get();
	      $param2a = ParameterPenilaian::where('Tipe', '2_0')->orderBy('ParameterID')->get();
	      $param2b = ParameterPenilaian::where('Tipe', '2_1')->orderBy('ParameterID')->get();
	      $param3a = ParameterPenilaian::where('Tipe', '3_0')->orderBy('ParameterID')->get();
	      $param3b = ParameterPenilaian::where('Tipe', '3_1')->orderBy('ParameterID')->get();
	      return view('seminar.daftarparameter', compact('param1', 'param2', 'param3', 'param1a','param2a','param3a','param1b','param2b','param3b'));
	    }
      return redirect()->action('HomeController@index');
    }
}