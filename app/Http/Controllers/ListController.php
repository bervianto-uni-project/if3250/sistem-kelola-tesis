<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\BimbinganController;

use App\Models\Mahasiswa;
use App\Models\Dosen;
use App\Models\MahasiswaDosbing;

class ListController extends Controller
{
    const DOSBING_APPROVED = 1;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listMahasiswa()
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'dosen') {
            $listEntity = MahasiswaDosbing::select('MahasiswaID as ID', 'mahasiswa.Nama')
                ->join('mahasiswa', 'mahasiswa.NIM', '=', 'MahasiswaID')
                ->where('DosenID', Auth::user()->username)
                ->where('mahasiswa_dosbing.Approved', self::DOSBING_APPROVED)
                ->get();
            return view('list.showList')
                ->with('listEntity', $listEntity)
                ->with('type', 'mahasiswa');

        } else if ($role == 'admin' || $role == 'kaprodi') {
            $listEntity = Mahasiswa::select('NIM as ID', 'Nama')
                ->get();
            return view('list.showList')
                ->with('listEntity', $listEntity)
                ->with('type', 'mahasiswa');
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function listDosen()
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin' || $role == 'kaprodi') {
            $listEntity = Dosen::select('NIP as ID', 'Nama')
                ->get();
            return view('list.showList')
                ->with('listEntity', $listEntity)
                ->with('type', 'dosen');
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function mahasiswaDetail($id)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin' || $role == 'kaprodi') {
            $mahasiswa = Mahasiswa::select('NIM', 'Nama', 'Email', 'Topik', 'Index', 'Approved')
                ->where('NIM', $id)
                ->first();
            return view('list.mahasiswa')
                ->with('mahasiswaDetail', $mahasiswa);
        } else if ($role == 'mahasiswa') {
            if ($id == Auth::user()->username) {
                $mahasiswa = Mahasiswa::select('NIM', 'Nama', 'Email', 'Topik', 'Index', 'Approved')
                    ->where('NIM', $id)
                    ->first();
                return view('list.mahasiswa')
                    ->with('mahasiswaDetail', $mahasiswa);
            } else {
                return redirect()->action('HomeController@index');
            }
        } else if ($role == 'dosen') {
            $mahasiswa = MahasiswaDosbing::select('MahasiswaID')
                ->where('DosenID', Auth::user()->username)
                ->where('MahasiswaID', $id)
                ->where('Approved', self::DOSBING_APPROVED)
                ->first();
            if ($mahasiswa == null) {
                return redirect()->action('HomeController@index');
            } else {
                $mahasiswa = Mahasiswa::select('NIM', 'Nama', 'Email', 'Topik', 'Index', 'Approved')
                    ->where('NIM', $id)
                    ->first();
                return view('list.mahasiswa')
                    ->with('mahasiswaDetail', $mahasiswa);
            }
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function dosenDetail($id)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin' || $role == 'kaprodi') {
            $dosen = Dosen::select('NIP', 'Nama', 'Email', 'Telepon')
                ->where('NIP', $id)
                ->first();
    
            $listMhsBimbingan = MahasiswaDosbing::select('MahasiswaID as ID', 'mahasiswa.Nama')
                ->join('mahasiswa', 'mahasiswa.NIM', '=', 'MahasiswaID')
                ->where('DosenID', $dosen->NIP)
                ->where('mahasiswa_dosbing.Approved', self::DOSBING_APPROVED)
                ->get();
    
            return view('list.dosen')
                ->with('dosenDetail', $dosen)
                ->with('listMhs', $listMhsBimbingan);
        } else if ($role == 'dosen') {
            if (Auth::user()->username == $id) {
                $dosen = Dosen::select('NIP', 'Nama', 'Email', 'Telepon')
                ->where('NIP', $id)
                ->first();
    
                $listMhsBimbingan = MahasiswaDosbing::select('MahasiswaID as ID', 'mahasiswa.Nama')
                    ->join('mahasiswa', 'mahasiswa.NIM', '=', 'MahasiswaID')
                    ->where('DosenID', $dosen->NIP)
                    ->where('mahasiswa_dosbing.Approved', self::DOSBING_APPROVED)
                    ->get();
        
                return view('list.dosen')
                    ->with('dosenDetail', $dosen)
                    ->with('listMhs', $listMhsBimbingan);
            } else {
                return redirect()->action('HomeController@index');
            }
        } else if ($role == 'mahasiswa') {
            $dosen = Dosen::select('NIP', 'Nama', 'Email', 'Telepon')
                ->where('NIP', $id)
                ->first();

             $mhsBimbingan = MahasiswaDosbing::select('MahasiswaID')
                ->where('DosenID', $dosen->NIP)
                ->where('MahasiswaID', Auth::user()->username)
                ->where('Approved', self::DOSBING_APPROVED)
                ->first();

             if ($mhsBimbingan != null) {
    
                $listMhsBimbingan = MahasiswaDosbing::select('MahasiswaID as ID', 'mahasiswa.Nama')
                    ->join('mahasiswa', 'mahasiswa.NIM', '=', 'MahasiswaID')
                    ->where('DosenID', $dosen->NIP)
                    ->where('mahasiswa_dosbing.Approved', self::DOSBING_APPROVED)
                    ->get();
        
                return view('list.dosen')
                    ->with('dosenDetail', $dosen)
                    ->with('listMhs', $listMhsBimbingan);
             } else {
                 return redirect()->action('HomeController@index');
             }
        } else {
            return redirect()->action('HomeController@index');
        }
    }
}
