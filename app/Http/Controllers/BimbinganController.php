<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Bimbingan;
use App\Models\Mahasiswa;
use App\Models\BimbinganDosbing;
use App\Models\MahasiswaDosbing;
use Session;

class BimbinganController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'mahasiswa') {
            //MAHASISWA
            $nim = Auth::user()->username;

            $mahasiswaDetail = Mahasiswa::select('Approved')
                ->where('NIM', $nim)
                ->first();
            if ($mahasiswaDetail->Approved != 1) {
                Session::put('errorText', 'Topik harus disetujui untuk melakukan bimbingan');
                return redirect()->action('HomeController@index');
            }

            $dosbing = MahasiswaDosbing::select('DosenID', 'Approved')
                ->where('MahasiswaID', $nim)
                ->get();
            foreach ($dosbing as $dosen) {
                if ($dosen->Approved != 1) {
                    Session::put('errorText', 'Semua dosen sudah harus menerima untuk melakukan bimbingan');
                    return redirect()->action('HomeController@index');
                }
            }
            return $this->mahasiswaBimbingan($nim);
        } else if ($role == 'dosen') {
            //DOSEN
            $dosenID = Auth::user()->username;
            return $this->dosenBimbingan($dosenID);
        } else if ($role == 'admin' || $role == 'kaprodi') {
            //ADMIN
            $bimbingans = Bimbingan::select('mahasiswa.Nama as Nama', 'ID', 'MahasiswaID', 'Tanggal', 'Rencana', 'Catatan')
                ->join('mahasiswa', 'mahasiswa.NIM', '=', 'MahasiswaID')
                ->orderBy('MahasiswaID')
                ->orderBy('ID', 'desc')
                ->get();
            $approveds = BimbinganDosbing::join('bimbingan', 'bimbingan.ID', '=', 'BimbinganID')
                ->leftjoin('dosen', 'bimbingan_dosbing.DosenID', '=', 'NIP')
                ->select('ID', 'dosen.Nama as Nama', 'Approved')
                ->orderBy('MahasiswaID')
                ->orderBy('ID', 'desc')
                ->get();
            $idx = 0;
            foreach ($bimbingans as $bimbingan) {
                while ($approveds[$idx]->ID == $bimbingan->ID) {
                    if ($approveds[$idx]->Approved == 1) {
                        $apv = array_merge((array)$bimbingan->Approved, array($approveds[$idx]->Approved));
                        $bimbingan->Approved = $apv;
                        $dos = array_merge((array)$bimbingan->Dosen, array($approveds[$idx]->Nama));
                        $bimbingan->Dosen = $dos;
                    }
                    if (isset($approveds[$idx + 1])) {
                        $idx++;
                    } else {
                        break;
                    }
                }
            }

            return view('bimbingan.admin')
                ->with('bimbingans', $bimbingans);
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function mahasiswaBimbingan($nim)
    {
        $bimbingans = Bimbingan::select('mahasiswa.Nama as Nama', 'MahasiswaID', 'ID', 'Tanggal', 'Catatan', 'Rencana')
            ->join('mahasiswa', 'mahasiswa.NIM', '=', 'MahasiswaID')
            ->where('MahasiswaID', $nim)
            ->orderBy('ID', 'desc')
            ->get();
        $approveds = BimbinganDosbing::join('bimbingan', 'bimbingan.ID', '=', 'BimbinganID')
            ->join('dosen', 'bimbingan_dosbing.DosenID', '=', 'NIP')
            ->select('ID', 'dosen.Nama as Nama', 'Approved')
            ->where('MahasiswaID', $nim)
            ->orderBy('ID', 'desc')
            ->get();
        $idx = 0;
        foreach ($bimbingans as $bimbingan) {
            while ($approveds[$idx]->ID == $bimbingan->ID) {
                if ($approveds[$idx]->Approved == 1) {
                    $apv = array_merge((array)$bimbingan->Approved, array($approveds[$idx]->Approved));
                    $bimbingan->Approved = $apv;
                    $dos = array_merge((array)$bimbingan->Dosen, array($approveds[$idx]->Nama));
                    $bimbingan->Dosen = $dos;
                }
                if (isset($approveds[$idx + 1])) {
                    $idx++;
                } else {
                    break;
                }
            }
        }

        return view('bimbingan.mahasiswa-table')
            ->with('bimbingans', $bimbingans);
    }

    public function dosenBimbingan($dosenID)
    {
        $mahasiswas = MahasiswaDosbing::join('mahasiswa', 'MahasiswaID', '=', 'mahasiswa.NIM')
            ->select('MahasiswaID', 'Nama')
            ->where('DosenID', $dosenID)->get();
        $i = 0;
        $bimbingans[$i] = '';
        foreach ($mahasiswas as $mahasiswa) {
            $bimbingans[$i] = Bimbingan::select('ID', 'Tanggal', 'Catatan', 'Rencana')
                ->where('MahasiswaID', $mahasiswa->MahasiswaID)->get();
            foreach ($bimbingans[$i] as $b) {
                $approveds = BimbinganDosbing::select('Approved')
                    ->where('DosenID', $dosenID)
                    ->where('BimbinganID', $b->ID)
                    ->get();

                $b->Approved = $approveds[0]->Approved;
            }
            $i = $i + 1;
        }
        return view('bimbingan.dosen-table')
            ->with('mahasiswas', $mahasiswas)
            ->with('bimbingans', $bimbingans);
    }


    public function create()
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'mahasiswa') {
            return view('bimbingan.createBimbingan');
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function detail($id)
    {
        if (is_numeric($id)) {
            $bimbingan = Bimbingan::select('ID', 'MahasiswaID', 'Tanggal', 'Catatan', 'Rencana')
                ->where('ID', $id)
                ->first();
            $approveds = BimbinganDosbing::select('DosenID', 'Approved', 'dosen.Nama as Nama')
                ->join('dosen', 'dosen.NIP', '=', 'DosenID')
                ->where('BimbinganID', $id)
                ->get();
            foreach ($approveds as $idx => $aprv) {
                $apv = array_merge((array)$bimbingan->Approved, array($aprv->Approved));
                $bimbingan->Approved = $apv;
                $dos = array_merge((array)$bimbingan->Dosen, array($approveds[$idx]->Nama));
                $bimbingan->Dosen = $dos;
                $dosID = array_merge((array)$bimbingan->DosenID, array($approveds[$idx]->DosenID));
                $bimbingan->DosenID = $dosID;
            }

            return view('bimbingan.detail')
                ->with('bimbingan', $bimbingan);
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function store(Request $request)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'mahasiswa') {
            $MahasiswaID = Auth::user()->username;
            $date = $request->tanggal;
            if (false === strtotime($date)) {
                return view('bimbingan.createBimbingan')
                    ->with('errorText', 'Masukkan tanggal sesuai format');
            } else {
                list($day, $month, $year) = explode('-', $date);
                if (false === checkdate($month, $day, $year)) {
                    return view('bimbingan.createBimbingan')
                        ->with('errorText', 'Tanggal tidak sesuai');
                }
            }

            $insertedDate = date("Y-m-d", strtotime($date));
            Bimbingan::create([
                'MahasiswaID' => $MahasiswaID,
                'Tanggal' => $insertedDate,
                'Catatan' => $request->isi,
                'Rencana' => $request->rencana
            ]);
            return redirect('bimbingan');
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function approve(Request $request)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'dosen') {
            BimbinganDosbing::where('DosenID', $request->DosenID)
                ->where('BimbinganID', $request->BimbinganID)
                ->update(['Approved' => 1]);

            return redirect('bimbingan');
        } else {
            return redirect()->action('HomeController@index');
        }
    }
}
