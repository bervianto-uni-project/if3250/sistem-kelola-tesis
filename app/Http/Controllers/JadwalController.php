<?php

namespace App\Http\Controllers;

use DateTimeZone;
use Illuminate\Http\Request;
use App\Models\Jadwal;
use App\Models\PengajuanJadwal;
use App\Models\Mahasiswa;
use App\Models\Dosen;
use App\Models\SeminarSidang;
use App\Models\SeminarInfo;
use App\Models\SeminarNilai;
use App\Models\ParameterPenilaian;
use App\Models\MahasiswaDosbing;
use App\Models\KategoriSeminarSidang;
use Illuminate\Support\Facades\Auth;
use DateTime;
use Session;
use DB;

class JadwalController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $role = strtolower(Auth::user()->role);
        $nim = Auth::user()->username;
        if ($role == 'admin') {
            $jadwals = Jadwal::join('kategori_seminar_sidang', 'Jenis_Kegiatan', '=', 'kategori_seminar_sidang.ID')
                ->select('jadwal.ID', 'Waktu_Mulai', 'Waktu_Akhir', 'Jenis as Jenis_Kegiatan', 'Keterangan')->get();
            $pjadwals = PengajuanJadwal::join('mahasiswa', 'MahasiswaID', '=', 'mahasiswa.NIM')
                ->join('kategori_seminar_sidang', 'Jenis_Kegiatan', '=', 'kategori_seminar_sidang.ID')
                ->select('pengajuan_jadwal.ID', 'Tanggal', 'Waktu_Awal', 'Waktu_Akhir', 'Jenis as Jenis_Kegiatan', 'Nama', 'Keterangan', 'pengajuan_jadwal.Approved')
                ->get();
            $spekjadwals = SeminarSidang::join('mahasiswa', 'MahasiswaID', '=', 'mahasiswa.NIM')
                ->select('ID', 'Tanggal', 'Waktu_Awal', 'Waktu_Akhir', 'Ruangan', 'Tipe', 'Keterangan', 'Nama')
                ->get();
        } else if ($role == 'mahasiswa') {
            $mahasiswaDetail = Mahasiswa::select('Approved')
                ->where('NIM', $nim)
                ->first();
            if ($mahasiswaDetail->Approved != 1) {
                Session::put('errorText', 'Topik harus disetujui untuk mengajukan jadwal');
                return redirect()->action('HomeController@index');
            }

            $jadwals = Jadwal::join('kategori_seminar_sidang', 'Jenis_Kegiatan', '=', 'kategori_seminar_sidang.ID')
                ->select('jadwal.ID', 'Waktu_Mulai', 'Waktu_Akhir', 'Jenis as Jenis_Kegiatan', 'Keterangan')->get();
            $pjadwals = PengajuanJadwal::join('mahasiswa', 'MahasiswaID', '=', 'mahasiswa.NIM')
                ->join('kategori_seminar_sidang', 'Jenis_Kegiatan', '=', 'kategori_seminar_sidang.ID')
                ->select('pengajuan_jadwal.ID', 'Tanggal', 'Waktu_Awal', 'Waktu_Akhir', 'Jenis as Jenis_Kegiatan', 'Nama', 'Keterangan', 'pengajuan_jadwal.Approved')
                ->where('MahasiswaID', $nim)
                ->get();
            $spekjadwals = SeminarSidang::join('kategori_seminar_sidang', 'Tipe', '=', 'kategori_seminar_sidang.ID')
                ->select('seminar_sidang.ID', 'Tanggal', 'Waktu_Awal', 'Waktu_Akhir', 'Ruangan', 'Jenis as Tipe', 'Keterangan', 'DosenAID', 'DosenBID', 'DosenCID')
                ->where('MahasiswaID', $nim)
                ->get();
            foreach ($spekjadwals as $spj) {
                $dosenA = Dosen::select('Nama')->where('NIP', $spj->DosenAID)->get();
                $spj->DosenA = $dosenA[0]->Nama;
                $dosenB = Dosen::select('Nama')->where('NIP', $spj->DosenBID)->get();
                $spj->DosenB = $dosenB[0]->Nama;
                $dosenC = Dosen::select('Nama')->where('NIP', $spj->DosenCID)->get();
                $spj->DosenC = $dosenC[0]->Nama;
            }
            return view('jadwal.jadwal')
                ->with('jadwals', $jadwals)
                ->with('pjadwals', $pjadwals)
                ->with('spekjadwals', $spekjadwals);
        } else if ($role == 'kaprodi') {
            $jadwals = Jadwal::join('kategori_seminar_sidang', 'Jenis_Kegiatan', '=', 'kategori_seminar_sidang.ID')
                ->select('jadwal.ID', 'Waktu_Mulai', 'Waktu_Akhir', 'Jenis as Jenis_Kegiatan', 'Keterangan')->get();
            return view('jadwal.jadwal')
                ->with('jadwals', $jadwals);
        } else if ($role == 'dosen') {
            $jadwals = Jadwal::join('kategori_seminar_sidang', 'Jenis_Kegiatan', '=', 'kategori_seminar_sidang.ID')
                ->select('jadwal.ID', 'Waktu_Mulai', 'Waktu_Akhir', 'Jenis as Jenis_Kegiatan', 'Keterangan')->get();
            $spekjadwalsA = SeminarSidang::join('mahasiswa', 'MahasiswaID', '=', 'mahasiswa.NIM')
                ->select('ID', 'Tanggal', 'Waktu_Awal', 'Waktu_Akhir', 'Ruangan', 'Tipe', 'Keterangan', 'Nama')
                ->where('DosenAID', $nim)
                ->get();
            $spekjadwalsB = SeminarSidang::join('mahasiswa', 'MahasiswaID', '=', 'mahasiswa.NIM')
                ->select('ID', 'Tanggal', 'Waktu_Awal', 'Waktu_Akhir', 'Ruangan', 'Tipe', 'Keterangan', 'Nama')
                ->where('DosenBID', $nim)
                ->orwhere('DosenCID', $nim)
                ->get();
            return view('jadwal.jadwal')
                ->with('jadwals', $jadwals)
                ->with('spekjadwalsA', $spekjadwalsA)
                ->with('spekjadwalsB', $spekjadwalsB);
        } else {
            return redirect()->action('HomeController@index');
        }

        return view('jadwal.jadwal')
            ->with('jadwals', $jadwals)
            ->with('pjadwals', $pjadwals)
            ->with('spekjadwals', $spekjadwals);
    }

    public function retMonth(Request $request)
    {
        $bulan = $request->bulan;
        $tahun = $request->tahun;
        $jadwals = Jadwal::select('ID', 'Waktu_Mulai', 'Waktu_Akhir', 'Kegiatan')
            ->whereMonth('Waktu_Mulai', $bulan)
            ->whereYear('Waktu_Mulai', $tahun)
            ->get();
        $pjadwals = PengajuanJadwal::join('mahasiswa', 'MahasiswaID', '=', 'mahasiswa.NIM')
            ->select('ID', 'Tanggal', 'Waktu', 'Kegiatan', 'Nama', 'pengajuan_jadwal.Approved')
            ->whereMonth('Tanggal', $bulan)
            ->whereYear('Tanggal', $tahun)
            ->get();
        return view('jadwal.jadwal-admin')
            ->with('jadwals', $jadwals)
            ->with('pjadwals', $pjadwals);
    }

    public function edit($id)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin') {
            if (is_numeric($id)) {
                $jadwal = Jadwal::join('kategori_seminar_sidang', 'Jenis_Kegiatan', '=', 'kategori_seminar_sidang.ID')
                    ->select('jadwal.ID', 'Waktu_Mulai', 'Waktu_Akhir', 'Jenis as Jenis_Kegiatan', 'Keterangan')
                    ->where('jadwal.ID', $id)
                    ->get();
                $listJenis = KategoriSeminarSidang::select('ID', 'Jenis')->get();
                return view('jadwal.editJadwal')
                    ->with('jadwal', $jadwal[0])
                    ->with('lj', $listJenis);
            } else {
                return redirect()->action('HomeController@index');
            }
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function view(Request $request)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin') {
            $id = $request->pjID;
            if (is_numeric($id)) {
                $pjadwals = PengajuanJadwal::join('mahasiswa', 'MahasiswaID', '=', 'mahasiswa.NIM')
                    ->join('kategori_seminar_sidang', 'Jenis_Kegiatan', '=', 'kategori_seminar_sidang.ID')
                    ->select('pengajuan_jadwal.ID', 'Tanggal', 'Waktu_Awal', 'Waktu_Akhir', 'Jenis as Jenis_Kegiatan', 'Nama', 'Keterangan', 'pengajuan_jadwal.Approved', 'MahasiswaID', 'kategori_seminar_sidang.ID as Jenis')
                    ->where('pengajuan_jadwal.ID', $id)
                    ->get();
                return view('jadwal.viewPengajuan')
                    ->with('pj', $pjadwals[0]);
            } else {
                return redirect()->action('HomeController@index');
            }
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function approve(Request $request)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin') {
            $id = $request->pjID;
            if (is_numeric($id)) {
                $pjadwals = PengajuanJadwal::join('mahasiswa', 'MahasiswaID', '=', 'mahasiswa.NIM')
                    ->join('kategori_seminar_sidang', 'Jenis_Kegiatan', '=', 'kategori_seminar_sidang.ID')
                    ->select('pengajuan_jadwal.ID', 'Tanggal', 'Waktu_Awal', 'Waktu_Akhir', 'Jenis as Jenis_Kegiatan', 'Nama', 'Keterangan', 'pengajuan_jadwal.Approved', 'MahasiswaID', 'kategori_seminar_sidang.ID as Jenis')
                    ->where('pengajuan_jadwal.ID', $id)
                    ->get();

                $listDosen = Dosen::select('NIP as ID', 'Nama')->get();


                return view('jadwal.jadwalApproval')
                    ->with('pj', $pjadwals[0])
                    ->with('listDosen', $listDosen);
            } else {
                return redirect()->action('HomeController@index');
            }
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function confirmApproval(Request $request)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin') {
            $id = $request->pjID;
            if (is_numeric($id)) {
                $pjadwals = PengajuanJadwal::join('mahasiswa', 'MahasiswaID', '=', 'mahasiswa.NIM')
                    ->join('kategori_seminar_sidang', 'Jenis_Kegiatan', '=', 'kategori_seminar_sidang.ID')
                    ->select('pengajuan_jadwal.ID', 'pengajuan_jadwal.jadwal_id', 'Tanggal', 'Waktu_Awal', 'Waktu_Akhir', 'Jenis as Jenis_Kegiatan', 'Nama', 'Keterangan', 'pengajuan_jadwal.Approved', 'MahasiswaID', 'kategori_seminar_sidang.ID as Jenis')
                    ->where('pengajuan_jadwal.ID', $id)
                    ->get();

                $dosbing = MahasiswaDosbing::select('DosenID')
                    ->where('MahasiswaID', $pjadwals[0]->MahasiswaID)
                    ->where('Approved', 1)
                    ->first();

                $pjadwals[0]->Ruangan = $request->ruangan;
                $pjadwals[0]->dosenA = $dosbing->DosenID;
                $pjadwals[0]->dosenB = $request->dosen2;
                $pjadwals[0]->dosenC = $request->dosen3;

                if (($request->dosen2 == $dosbing->DosenID) || ($request->dosen3 == $dosbing->DosenID)){
                    return $this->approve($request)
                        ->with('errorText', 'Dosen pembimbing tidak boleh menjadi dosen penguji seminar/sidang');
                } else if ($request->dosen2 == $request->dosen3){
                    return $this->approve($request)
                        ->with('errorText', 'Dosen Penguji 1 dan Dosen Penguji 2 tidak boleh sama');
                }

                return view('jadwal.confirmApproval')
                    ->with('pj', $pjadwals[0]);
            } else {
                return redirect()->action('HomeController@index');
            }
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function decline(Request $request)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin') {
            $id = $request->pjID;
            if (is_numeric($id)) {
                PengajuanJadwal::where('ID', $id)
                    ->update(['Approved' => '-1']);
                return redirect()->action('JadwalController@index');
            } else {
                return redirect()->action('HomeController@index');
            }
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function create()
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin') {
            $listJenis = KategoriSeminarSidang::select('ID', 'Jenis')->get();
            return view('jadwal.createJadwal')
                ->with('lj', $listJenis);
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function pengajuanJadwal($id)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'mahasiswa') {
            if (is_numeric($id)) {
                $jadwal = Jadwal::join('kategori_seminar_sidang', 'Jenis_Kegiatan', '=', 'kategori_seminar_sidang.ID')
                    ->select('jadwal.ID', 'Waktu_Mulai', 'Waktu_Akhir', 'Jenis as Jenis_Kegiatan', 'Keterangan', 'kategori_seminar_sidang.ID as Jenis')
                    ->where('jadwal.ID', $id)
                    ->get();
                if (!isset($errorText)) {
                    return view('jadwal.pengajuanJadwal')
                        ->with('jadwal', $jadwal[0]);
                } else {
                    return view('jadwal.pengajuanJadwal')
                        ->with('jadwal', $jadwal[0])
                        ->with('errorText', 'Masukkan tanggal sesuai format');
                }
            } else {
                return redirect()->action('HomeController@index');
            }
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function editSeminarSidang($id)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin') {
            if (is_numeric($id)) {
                $spekjadwals = SeminarSidang::join('mahasiswa', 'MahasiswaID', '=', 'mahasiswa.NIM')
                    ->join('kategori_seminar_sidang', 'Tipe', '=', 'kategori_seminar_sidang.ID')
                    ->select('seminar_sidang.ID', 'Tanggal', 'Waktu_Awal', 'Waktu_Akhir', 'Ruangan', 'Keterangan', 'Nama', 'DosenAID', 'DosenBID', 'DosenCID', 'Jenis as Tipe')
                    ->where('seminar_sidang.ID', $id)
                    ->get();
                return view('jadwal.editSeminarSidang')
                    ->with('sj', $spekjadwals[0]);
            } else {
                return redirect()->action('HomeController@index');
            }
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function updateSeminarSidang(Request $request)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin') {
            $id = $request->idSeminar;
            if (is_numeric($id)) {
                $date = $request->tanggal;
                if (false === strtotime($date)) {
                    return $this->editSeminarSidang($request->idSeminar)
                        ->with('errorText', 'Masukkan tanggal sesuai format');
                }
                list($hour, $minute, $second) = explode(':', $request->waktu_aw);
                if (!(0 <= $hour && $hour <= 23 && 0 <= $minute && $minute <= 59 && 0 <= $second && $second <= 59)) {
                    return $this->editSeminarSidang($request->idSeminar)
                        ->with('errorText', 'Masukkan waktu awal sesuai format');
                }
                list($hour, $minute, $second) = explode(':', $request->waktu_a);
                if (!(0 <= $hour && $hour <= 23 && 0 <= $minute && $minute <= 59 && 0 <= $second && $second <= 59)) {
                    return $this->editSeminarSidang($request->idSeminar)
                        ->with('errorText', 'Masukkan waktu akhir sesuai format');
                }

                $insertedDate = date("Y-m-d", strtotime($date));

                $waktu_aw = date("h:i:s", strtotime($request->waktu_aw));
                $waktu_a = date("h:i:s", strtotime($request->waktu_a));


                $wkt_awal = new DateTime($waktu_aw);
                $wkt_akhir = new DateTime($waktu_a);

                $difference = $wkt_awal->diff($wkt_akhir);

                $differ = $difference->h * 60 + $difference->i;

                if (($waktu_aw >= $waktu_a) || ($differ < 90)) {
                    return $this->editSeminarSidang($request->idSeminar)
                        ->with('errorText', 'Durasi waktu minimal 90 menit');
                }

                if (($request->dosenB == $request->dosbing) || ($request->dosenC == $request->dosbing)){
                    return $this->editSeminarSidang($request->idSeminar)
                        ->with('errorText', 'Dosen pembimbing tidak boleh menjadi dosen penguji seminar/sidang');
                } else if ($request->dosenB == $request->dosenC){
                    return $this->editSeminarSidang($request->idSeminar)
                        ->with('errorText', 'Dosen Penguji 1 dan Dosen Penguji 2 tidak boleh sama');
                }

                $info = SeminarSidang::where('ID', $id)->first();

                SeminarNilai::where('SeminarID', $id)
                    ->where('DosenID', $info->DosenBID)
                    ->update(['DosenID', $request->dosenB]);

                SeminarNilai::where('SeminarID', $id)
                    ->where('DosenID', $info->DosenCID)
                    ->update(['DosenID', $request->dosenC]);

                SeminarSidang::where('ID', $id)
                    ->update([
                        'Tanggal' => $insertedDate,
                        'Waktu_Awal' => $waktu_aw,
                        'Waktu_Akhir' => $waktu_a,
                        'Keterangan' => $request->keterangan,
                        'Ruangan' => $request->ruangan,
                        'DosenBID' => $request->dosenB,
                        'DosenCID' => $request->dosenC
                    ]);

                return redirect()->action('JadwalController@index');
            } else {
                return redirect()->action('HomeController@index');
            }
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function store(Request $request)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin') {
            $waktu_m = $request->waktu_m;
            $waktu_a = $request->waktu_a;

            if (false === strtotime($waktu_m)) {
                return view('jadwal.createjadwal')
                    ->with('errorText', 'Masukkan tanggal sesuai format');
            }
            if (false === strtotime($waktu_a)) {
                return view('jadwal.createjadwal')
                    ->with('errorText', 'Masukkan tanggal sesuai format');
            }

            $waktu_mulai = date("Y-m-d", strtotime($waktu_m));
            $waktu_akhir = date("Y-m-d", strtotime($waktu_a));

            if ($waktu_mulai > $waktu_akhir) {
                return view('jadwal.createjadwal')
                    ->with('errorText', 'Waktu mulai harus lebih kecil dari waktu akhir');
            }

            Jadwal::create([
                'Waktu_Mulai' => $waktu_mulai,
                'Waktu_Akhir' => $waktu_akhir,
                'Jenis_Kegiatan' => $request->jenis,
                'Keterangan' => $request->keterangan
            ]);
            return redirect()->action('JadwalController@index');
        } else if ($role == 'mahasiswa') {
            $date = $request->tanggal;
            if (false === strtotime($date)) {
                return $this->pengajuanJadwal($request->idJadwal)
                    ->with('errorText', 'Masukkan tanggal sesuai format');
            }
            list($hour, $minute, $second) = explode(':', $request->waktu_aw);
            if (!(0 <= $hour && $hour <= 23 && 0 <= $minute && $minute <= 59 && 0 <= $second && $second <= 59)) {
                return $this->pengajuanJadwal($request->idJadwal)
                    ->with('errorText', 'Masukkan waktu awal sesuai format');
            }
            list($hour, $minute, $second) = explode(':', $request->waktu_a);
            if (!(0 <= $hour && $hour <= 23 && 0 <= $minute && $minute <= 59 && 0 <= $second && $second <= 59)) {
                return $this->pengajuanJadwal($request->idJadwal)
                    ->with('errorText', 'Masukkan waktu akhir sesuai format');
            }

            $insertedDate = date("Y-m-d", strtotime($date));

            $waktu_aw = date("h:i:s", strtotime($request->waktu_aw));
            $waktu_a = date("h:i:s", strtotime($request->waktu_a));


            $wkt_awal = new DateTime($waktu_aw, new DateTimeZone('Asia/Jakarta'));
            $wkt_akhir = new DateTime($waktu_a, new DateTimeZone('Asia/Jakarta'));

            $difference = $wkt_awal->diff($wkt_akhir);

            $differ = $difference->h * 60 + $difference->i;

            if (($waktu_aw >= $waktu_a) || ($differ < 90)) {
                return $this->pengajuanJadwal($request->idJadwal)
                    ->with('errorText', 'Durasi waktu minimal 90 menit');
            }

            if (($insertedDate < $request->tgl_awal) || ($insertedDate > $request->tgl_akhir)) {
                return $this->pengajuanJadwal($request->idJadwal)
                    ->with('errorText', 'Harus dalam tanggal yang ditentukan');
            }


            PengajuanJadwal::create([
                'jadwal_id' => $request->idJadwal,
                'Tanggal' => $insertedDate,
                'Waktu_Awal' => $request->waktu_aw,
                'Waktu_Akhir' => $request->waktu_a,
                'Jenis_Kegiatan' => $request->jenis,
                'Keterangan' => $request->keterangan,
                'MahasiswaID' => Auth::user()->username,
                'Approved' => '0'
            ]);
            return redirect()->action('JadwalController@index');
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function postApproval(Request $request)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin') {
            $id = $request->pjID;
            if (is_numeric($id)) {
                PengajuanJadwal::where('ID', $id)
                    ->update(['Approved' => '1']);

                $jadwal_id = PengajuanJadwal::select('pengajuan_jadwal.jadwal_id')->where('ID', $id)->first();

                SeminarSidang::create([
                    'jadwal_id' => $jadwal_id->jadwal_id,
                    'permintaan_jadwal_id' => $id,
                    'Tanggal' => $request->tanggal,
                    'Waktu_Awal' => $request->waktu_aw,
                    'Waktu_Akhir' => $request->waktu_a,
                    'Ruangan' => $request->ruangan,
                    'Keterangan' => $request->keterangan,
                    'MahasiswaID' => $request->nim,
                    'DosenAID' => $request->dosen1,
                    'DosenBID' => $request->dosen2,
                    'DosenCID' => $request->dosen3,
                    'Tipe' => $request->jenis
                ]);

                $newid = DB::table('seminar_sidang')->max('ID');

                SeminarInfo::create([
                    'SeminarID' => $newid,
                    'Nilai' => '-',
                    'Active' => 0
                    ]);

                $paramlist = ParameterPenilaian::select('Tipe','ParameterID')->where('Tipe', 'like', $request->jenis . '_%')->get();
                foreach ($paramlist as $param) {
                    if ($param->ParameterID == '-1') {
                        SeminarNilai::create([
                        'SeminarID' => $newid,
                        'Tipe' => $request->jenis . '_1',
                        'DosenID' => '-',
                        'ParameterID' => '-1',
                        'ParamValue' => 0
                    ]);
                    } else {
                        if ($param->Tipe == $request->jenis . '_0') {
                        SeminarNilai::create([
                            'SeminarID' => $newid,
                            'Tipe' => $request->jenis . '_0',
                            'DosenID' => $request->dosen1,
                            'ParameterID' => $param->ParameterID,
                            'ParamValue' => 0
                            ]);
                        } else if ($param->Tipe == $request->jenis . '_1') {
                        SeminarNilai::create([
                            'SeminarID' => $newid,
                            'Tipe' => $request->jenis . '_1',
                            'DosenID' => $request->dosen2,
                            'ParameterID' => $param->ParameterID,
                            'ParamValue' => 0
                            ]);
                        SeminarNilai::create([
                            'SeminarID' => $newid,
                            'Tipe' => $request->jenis . '_1',
                            'DosenID' => $request->dosen3,
                            'ParameterID' => $param->ParameterID,
                            'ParamValue' => 0
                            ]);
                        }
                    }
                }
                return redirect()->action('JadwalController@index');
            } else {
                return redirect()->action('HomeController@index');
            }
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function updateData($id, Request $request)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin') {
            $waktu_m = $request->waktu_m;
            $waktu_a = $request->waktu_a;

            if (false === strtotime($waktu_m)) {
                return $this->edit($id)
                    ->with('errorText', 'Masukkan tanggal sesuai format');
            }

            if (false === strtotime($waktu_a)) {
                return $this->edit($id)
                    ->with('errorText', 'Masukkan tanggal sesuai format');
            }

            $insertedDate_m = date("Y-m-d", strtotime($waktu_m));
            $insertedDate_a = date("Y-m-d", strtotime($waktu_a));

            $kegiatan = KategoriSeminarSidang::select('ID')
                ->where('Jenis', $request->jenis)
                ->get();

            Jadwal::where('ID', $id)
                ->update([
                    'Waktu_Mulai' => $insertedDate_m,
                    'Waktu_Akhir' => $insertedDate_a,
                    'Jenis_Kegiatan' => $kegiatan[0]->ID,
                    'Keterangan' => $request->keterangan,
                ]);
            return redirect()->action('JadwalController@index');
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    public function deleteData($id)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'admin') {
            Jadwal::where('ID', $id)->delete();
            return redirect()->action('JadwalController@index');
        } else {
            return redirect()->action('HomeController@index');
        }
    }
}
