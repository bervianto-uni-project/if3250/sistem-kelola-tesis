<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Mahasiswa;
use App\Models\MahasiswaDosbing;
use App\Models\Dosen;
use Session;

class HomeController extends Controller
{
    public $role = 'default';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == 'Mahasiswa') {
            return $this->mahasiswaIndex();
        } else if (Auth::user()->role == 'Dosen') {
            return $this->dosenIndex();
        } else {
            return view('home.home-admin');
        }
    }

    public function mahasiswaIndex()
    {
        $nim = Auth::user()->username;
        $mahasiswaDetail = Mahasiswa::select('NIM', 'Nama', 'Email', 'Topik', 'Index', 'Approved')
            ->where('NIM', $nim)
            ->get();
        $dosbing = MahasiswaDosbing::select('dosen.Nama as Nama', 'DosenID', 'Approved')
            ->join('dosen', 'dosen.NIP', '=', 'DosenID')
            ->where('MahasiswaID', $nim)
            ->get();

        $namaDosen = array();
        if (!$dosbing->isEmpty()) {
            foreach ($dosbing as $dos) {
                if ($dos->Approved == 0) {
                    $dos->msg = 'Belum diterima';
                } else if ($dos->Approved == 1) {
                    $dos->msg = 'Sudah diterima';
                } else if ($dos->Approved == -1) {
                    $dos->msg = 'Ditolak';
                }
            }
        }
        $listDosen = Dosen::select('NIP as ID', 'Nama')->get();
        $errorText = session('errorText');
        Session::put('errorText', '');

        return view('home.home-students')
            ->with('mahasiswaDetail', $mahasiswaDetail[0])
            ->with('dosbing', $dosbing)
            ->with('listDosen', $listDosen)
            ->with('errorText', $errorText);
    }

    public function dosenIndex()
    {
        $nip = Auth::user()->username;
        $dosenDetail = Dosen::select('Nama', 'Email', 'Telepon')
            ->where('NIP', $nip)
            ->get();

        $mahasiswa = MahasiswaDosbing::select('MahasiswaID', 'Approved')
            ->where('Approved', '!=', -1)
            ->where('DosenID', $nip)
            ->orderBy('Approved', 'asc')
            ->get();

        $namaMahasiswa = array();
        if (!$mahasiswa->isEmpty()) {
            foreach ($mahasiswa as $mhs) {
                $detail = Mahasiswa::select('Nama', 'Topik', 'Approved')
                    ->where('NIM', $mhs->MahasiswaID)
                    ->first();
                array_push($namaMahasiswa, $detail);
            }
        }

        return view('home.home-dosen')
            ->with('dosenDetail', $dosenDetail[0])
            ->with('mahasiswa', $mahasiswa)
            ->with('namaMahasiswa', $namaMahasiswa);
    }


    public function addDosenPembimbing(Request $request)
    {
        if (Auth::user()->role == 'Mahasiswa') {
            try {
                $mahasiswaID = Auth::user()->username;
                $dosenID = $request->NIPDosen;
                $Approved = 0;
                $dosenCheck = Dosen::where('NIP', $dosenID)
                    ->get();

                if (!$dosenCheck->isEmpty()) {
                    MahasiswaDosbing::create([
                        'MahasiswaID' => $mahasiswaID,
                        'DosenID' => $dosenID,
                        'Approved' => $Approved]);
                    return redirect()->action('HomeController@index');
                } else {
                    Session::put('errorText', 'Dosen tidak ditemukan');
                    return redirect()->action('HomeController@index');
                }


            } catch (\Illuminate\Database\QueryException $e) {
                Session::put('errorText', 'Tidak dapat menambahkan dosen yang sama');
                return redirect()->action('HomeController@index');
            }
        }
    }

    public function editTopik(Request $request)
    {
        if (Auth::user()->role == 'Mahasiswa') {
            $mahasiswaID = Auth::user()->username;

            $dosbing = MahasiswaDosbing::where('MahasiswaID', $mahasiswaID)
                ->where('Approved', 1)
                ->get();

            if (!$dosbing->isEmpty()) {
                Mahasiswa::where('NIM', $mahasiswaID)
                    ->update(['Topik' => $request->Topik, 'Approved' => 0]);

                return redirect()->action('HomeController@index');
            } else {
                Session::put('errorText', 'Harus sudah diterima minimal 1 dosen pembimbing');
                return redirect()->action('HomeController@index');
            }
        }
    }

    public function approveMhs(Request $request)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'dosen') {
            if ($request->option == 'Approve') {
                $approved = 1;
            } else {
                $approved = -1;
            }
            MahasiswaDosbing::where('DosenID', Auth::user()->username)
                ->where('MahasiswaID', $request->MahasiswaID)
                ->update(['Approved' => $approved]);
        }
        return redirect()->action('HomeController@index');
    }

    public function deleteReqDos(Request $request)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'mahasiswa') {
            MahasiswaDosbing::where('DosenID', $request->DosenID)
                ->where('MahasiswaID', Auth::user()->username)
                ->delete();
        }
        return redirect()->action('HomeController@index');
    }

    public function approveTopik(Request $request)
    {
        $role = strtolower(Auth::user()->role);
        if ($role == 'dosen') {
            if ($request->option == 'Approve') {
                Mahasiswa::where('NIM', $request->MahasiswaID)
                    ->update(['Approved' => 1]);
            } else {
                Mahasiswa::where('NIM', $request->MahasiswaID)
                    ->update(['Topik' => '', 'Approved' => -1]);
            }
        }
        return redirect()->action('HomeController@index');
    }


}
