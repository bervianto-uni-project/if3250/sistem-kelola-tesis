<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Dosen;
use App\Models\Mahasiswa;
use App\Models\SeminarSidang;
use App\Models\SeminarNilai;
use App\Models\SeminarInfo;
use App\Models\ParameterPenilaian;
use App\Models\ParameterDetail;
use App\Models\ParameterOption;
use DB;

class SeminarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show ($id) {
        if (Auth::user()->role == 'Dosen') {
            $user = Auth::user()->username;
            //$user = 195206131979031004;
        	$info = SeminarInfo::join('seminar_sidang','SeminarID','=','seminar_sidang.ID')
                ->where('seminar_sidang.ID', $id)
                ->first();
            $param1 = ParameterPenilaian::where('Tipe',$info->Tipe . '_0')->where('ChildSum',0)->orderBy('ParameterID')->get();
            $param2 = ParameterPenilaian::where('Tipe',$info->Tipe . '_1')->where('ChildSum',0)->orderBy('ParameterID')->get();
            $pembimbing = collect([]);
            $penguji1 = collect([]);
            $penguji2 = collect([]);

            foreach ($param1 as $p) {
                $nilaiset = SeminarNilai::where('Tipe',$info->Tipe . '_0')
                ->where('SeminarID',$id)
                ->where('DosenID',$info->DosenAID)
                ->where('ParameterID',$p->ParameterID)
                ->first();
                $pembimbing->push($nilaiset);
            } 

            foreach ($param2 as $p) {
                $nilaiset = SeminarNilai::where('Tipe',$info->Tipe . '_1')
                ->where('SeminarID',$id)
                ->where('DosenID',$info->DosenBID)
                ->where('ParameterID',$p->ParameterID)
                ->first();
                $penguji1->push($nilaiset);
            } 

            foreach ($param2 as $p) {
                $nilaiset = SeminarNilai::where('Tipe',$info->Tipe . '_1')
                ->where('SeminarID',$id)
                ->where('DosenID',$info->DosenCID)
                ->where('ParameterID',$p->ParameterID)
                ->first();
                $penguji2->push($nilaiset);
            }
            
            $name = array();
            $name[] = Dosen::where('NIP',$info->DosenAID)->first()->Nama;
            $name[] = Dosen::where('NIP',$info->DosenBID)->first()->Nama;
            $name[] = Dosen::where('NIP',$info->DosenCID)->first()->Nama;

            if ($user == $info->DosenAID)
                $catatan = $info->Catatan_1;
            else if ($user == $info->DosenBID)
                $catatan = $info->Catatan_2;
            else if ($user == $info->DosenCID)
                $catatan = $info->Catatan_3;

            $nilai = SeminarNilai::where('SeminarID',$id)->where('ParameterID','0')->get();

            $indeks = $info->Nilai;

        	return view('seminar.seminarsession', compact('user','pembimbing','penguji1','penguji2','param1','param2','name','info','catatan','nilai','indeks'))->with('id',$id);
        }
        return redirect()->action('HomeController@index');
    }

    public function update(Request $request) {
        if (Auth::user()->role == 'Dosen') {

            $param_options = ParameterOption::select('NilaiParameter', 'NamaParameter')->get();
            foreach ($param_options as $param_option) {
                $options[$param_option->NilaiParameter] = $param_option->NamaParameter;
            }

            $user = Auth::user()->username;
            //$user = 43515001;
            $info = SeminarInfo::join('seminar_sidang','SeminarID','=','seminar_sidang.ID')
                ->where('seminar_sidang.ID', $request->seminar_id)
                ->first();
            $tipe = strval($info->Tipe) . $request->tipe;
            $param = ParameterPenilaian::where('Tipe',$tipe)->where('ChildSum',0)->orderBy('ParameterID')->get();
            $nilai = collect([]);
            foreach ($param as $p) {
                $nilai->push(SeminarNilai::where('SeminarID',$request->seminar_id)
                    ->where('Tipe',$tipe)
                    ->where('DosenID',$user)
                    ->where('ParameterID',$p->ParameterID)
                    ->first());
            }
            $input = $request->value_select;
            foreach ($nilai as $i => $val) {
                $test = DB::table('seminar_nilai')
                ->where('SeminarID', $request->seminar_id)
                ->where('DosenID', $user)
                ->where('ParameterID', $val->ParameterID)
                ->update(['ParamValue' => $input[$i]]);
            }
            if ($request->catatan != null) {
                if($user == $info->DosenAID)
                    $info->Catatan_1 = $request->catatan;
                else if($user == $info->DosenBID)
                    $info->Catatan_2 = $request->catatan;
                else if($user == $info->DosenCID)
                    $info->Catatan_3 = $request->catatan;
                $info->save();
            }

            $param = ParameterPenilaian::where('Tipe',$tipe)->orderBy('ParameterID')->get();
            $nilai = SeminarNilai::where('SeminarID',$request->seminar_id)->where('Tipe',$tipe)->get();
            $max = $param->max('Level');
            for ($i = $max; $i >= -2; $i--) {
                if ($i < 0) {
                    $values = ParameterPenilaian::where('Tipe','like',strval($info->Tipe) . '%')->where('Level',$i)->get();
                } else {
                    $values = ParameterPenilaian::where('Tipe',$tipe)->orderBy('ParameterID')->where('Level',$i)->get();
                }
                $score = 1;
                foreach ($values as $v) {
                    if ($i >= 0) {
                        $detail = ParameterDetail::where('Tipe',$tipe)->where('ParameterID',$v->ParameterID)->get();
                        $temp = ParameterPenilaian::where('Tipe',$tipe)->orderBy('ParameterID')->where('Parent',$v->ParameterID)->get();
                        $paramlist = collect([]);
                        foreach ($temp as $t) {
                            $temp2 = SeminarNilai::where('SeminarID',$request->seminar_id)
                            ->where('Tipe',$tipe)
                            ->where('DosenID',$user)
                            ->where('ParameterID',$t->ParameterID)
                            ->first();
                            $paramlist->push($temp2);
                        }
                    } else {
                        $detail = ParameterDetail::where('Tipe','like',strval($info->Tipe) . '%')->where('ParameterID',$v->ParameterID)->get();
                        $temp = ParameterPenilaian::where('Tipe','like',strval($info->Tipe) . '%')->where('Parent',$v->ParameterID)->get();
                        $paramlist = collect([]);
                        foreach ($temp as $t) {
                            if ($i == -1) {
                                $temp2 = SeminarNilai::where('SeminarID',$request->seminar_id)
                                ->where('Tipe',$t->Tipe)
                                ->where('ParameterID',$t->ParameterID)
                                ->get();
                                foreach ($temp2 as $t2) {
                                    $paramlist->push($t2);
                                }
                            } else {
                                $temp2 = SeminarNilai::where('SeminarID',$request->seminar_id)
                                ->where('Tipe',$t->Tipe)
                                ->where('ParameterID',$t->ParameterID)
                                ->first();
                                $paramlist->push($temp2);
                            }
                        }
                    }
                    foreach ($detail as $d) {
                        $kriteria = explode('-',$d->Kriteria);
                        $j = 0;
                        $check = TRUE;
                        while ($check && $j < count($paramlist)) {
                            switch ($paramlist[$j]->ParamValue) {
                                case 0:
                                    if ($kriteria[$j] != $options[$paramlist[$j]->ParamValue])
                                        $check = FALSE;
                                    else
                                        $j++;
                                    break;
                                case 1:
                                    if ($kriteria[$j] != $options[$paramlist[$j]->ParamValue])
                                        $check = FALSE;
                                    else
                                        $j++;
                                    break;
                                case 2:
                                    if ($kriteria[$j] != $options[$paramlist[$j]->ParamValue])
                                        $check = FALSE;
                                    else
                                        $j++;
                                    break;
                                default:
                                    break;
                            }
                        }
                        if ($j == count($paramlist)) {
                            if ($i == -2) {
                                $change = SeminarInfo::where('SeminarID',$request->seminar_id)
                                ->update(['Nilai' => $d->HasilParameter]);
                            } else {
                                if ($d->HasilParameter == $options[0])
                                    $score = 0;
                                else if ($d->HasilParameter == $options[1])
                                    $score = 1;
                                else
                                    $score = 2;
                                if ($i == -1) {
                                    $change = SeminarNilai::where('SeminarID',$request->seminar_id)
                                    ->where('Tipe',$v->Tipe)
                                    ->where('ParameterID',$v->ParameterID)
                                    ->update(['ParamValue' => $score]);
                                } else {
                                    $change = SeminarNilai::where('SeminarID',$request->seminar_id)
                                    ->where('Tipe',$tipe)
                                    ->where('DosenID',$user)
                                    ->where('ParameterID',$v->ParameterID)
                                    ->update(['ParamValue' => $score]);
                                }
                            }
                        }
                    }
                }           
            }
            return redirect()->route('seminarsession', ['id' => $request->seminar_id]);
            //return $paramlist;
        }
        return redirect()->action('HomeController@index');
    }

    public function seminarlist() {
        if (Auth::user()->role == 'Admin' || Auth::user()->role == 'Kaprodi') {
            $list = SeminarInfo::join('seminar_sidang','SeminarID','=','seminar_sidang.ID')
                ->get();
            return view('seminar.listseminar', compact('list'));
        }
        return redirect()->action('HomeController@index');
    }

    public function change($id) {
        if (Auth::user()->role == 'Admin') {
            $state = SeminarInfo::where('SeminarID',$id)->first();
            if ($state->Active == 1)
                $state->Active = 0;
            else
                $state->Active = 1;
            $state->save();
            return redirect('listseminar');
        }
        return redirect()->action('HomeController@index');
    }

    public function available() {
        if (Auth::user()->role == 'Dosen') {
            $user = Auth::user()->username;
            //$user = '43515000';
            $list = SeminarInfo::join('seminar_sidang','SeminarID','=','seminar_sidang.ID')
                ->where(function ($query) use ($user) {
                    $query->where('DosenAID',$user)
                          ->orWhere('DosenBID',$user)
                          ->orWhere('DosenCID',$user);
                })->where('Active',1)->get();
            $topik = array();
            foreach ($list as $info) {
                $topik[] = Mahasiswa::where('NIM',$info->MahasiswaID)->first()->Topik;
            }
            return view('seminar.availableseminar', compact('list','topik'));
        }
        return redirect()->action('HomeController@index');
    }

    public function detail($id) {
        if (Auth::user()->role == 'Admin') {
            $info = SeminarInfo::join('seminar_sidang','SeminarID','=','seminar_sidang.ID')
                ->select('seminar_sidang.Tipe', 'Nilai','seminar_sidang.DosenBID','seminar_sidang.DosenCID')
                ->where('seminar_sidang.ID', $id)
                ->first();
            if ($info != null) {
                $param = ParameterPenilaian::where('Tipe', $info->Tipe)->first();
                $param1 = ParameterPenilaian::where('Tipe', 'like', $info->Tipe . '_0')->orderBy('ParameterID')->get();
                $param2 = ParameterPenilaian::where('Tipe', 'like', $info->Tipe . '_1')->orderBy('ParameterID')->get();
                $nilai1 = SeminarNilai::where('SeminarID', $id)->where('Tipe', 'like', $info->Tipe . '_0')->orderBy('ParameterID')->get();
                $nilai2a = SeminarNilai::where('SeminarID', $id)
                    ->where('Tipe', 'like', $info->Tipe . '_1')
                    ->where('DosenID', $info->DosenBID)
                    ->orderBy('ParameterID')
                    ->get();
                $nilai2b = SeminarNilai::where('SeminarID', $id)
                    ->where('Tipe', 'like', $info->Tipe . '_1')
                    ->where('DosenID', $info->DosenCID)
                    ->orderBy('ParameterID')
                    ->get();
                $nilai2c = SeminarNilai::where('SeminarID', $id)
                    ->where('Tipe', 'like', $info->Tipe . '_1')
                    ->where('DosenID', '-')
                    ->first();
                return view('seminar.seminardetail', compact('info', 'param', 'param1', 'param2', 'nilai1', 'nilai2a', 'nilai2b', 'nilai2c'))
                    ->with('id', $id);
                    // return $nilai2a;
            } else {
                return redirect()->action('SeminarController@seminarlist');
            }
        }
        return redirect()->action('HomeController@index');
    }

    
    public function detailupdate(Request $request) {
        if (Auth::user()->role == 'Admin') {
            $param_options = ParameterOption::select('NilaiParameter', 'NamaParameter')->get();
            foreach ($param_options as $param_option) {
                $options[$param_option->NilaiParameter] = $param_option->NamaParameter;
            }

            $id = $request->seminar_id;
            $info = SeminarInfo::join('seminar_sidang','SeminarID','=','seminar_sidang.ID')
                ->select('seminar_sidang.Tipe', 'Nilai','seminar_sidang.DosenBID','seminar_sidang.DosenCID')
                ->where('seminar_sidang.ID', $id)
                ->first();
            $nilai1 = $request->nilai1;
            $nilai2 = $request->nilai2;
            $update = SeminarInfo::where('SeminarID',$id)
                ->update(['Nilai' => $nilai1[0]]);
            $values = SeminarNilai::where('SeminarID',$id)->where('Tipe',$info->Tipe . '_0')->orderBy('ParameterID')->get();
            foreach ($values as $i => $v) {
                if ($nilai1[$i+1] == $options[0])
                    $n = 0;
                elseif ($nilai1[$i+1] == $options[1])
                    $n = 1;
                elseif ($nilai1[$i+1] == $options[2])
                    $n = 2;
                $updt = SeminarNilai::where('SeminarID',$id)->where('Tipe',$info->Tipe . '_0')->where('ParameterID',$v->ParameterID)
                ->update(['ParamValue' => $n]);
            }
            if ($nilai2[0] == $options[0])
                    $n = 0;
                elseif ($nilai2[0] == $options[1])
                    $n = 1;
                elseif ($nilai2[0] == $options[2])
                    $n = 2;
            $update = SeminarNilai::where('SeminarID',$id)->where('Tipe',$info->Tipe . '_1')->where('ParameterID','-1')
                ->update(['ParamValue' => $n]);
            $values = SeminarNilai::where('SeminarID',$id)->where('Tipe',$info->Tipe . '_1')->orderBy('ParameterID')->get();
            foreach ($values as $i => $v) {
                if ($i % 2 == 1) {
                    if ($nilai2[$i] == $options[0])
                            $n = 0;
                    elseif ($nilai2[$i] == $options[1])
                        $n = 1;
                    elseif ($nilai2[$i] == $options[2])
                        $n = 2;
                    $updt = SeminarNilai::where('SeminarID',$id)
                    ->where('Tipe',$info->Tipe . '_1')
                    ->where('DosenID', $info->DosenBID)
                    ->where('ParameterID',$v->ParameterID)
                    ->update(['ParamValue' => $n]);
                    if ($nilai2[$i+1] == $options[0])
                        $n = 0;
                    elseif ($nilai2[$i+1] == $options[1])
                        $n = 1;
                    elseif ($nilai2[$i+1] == $options[2])
                        $n = 2;
                    $updt = SeminarNilai::where('SeminarID',$id)
                    ->where('Tipe',$info->Tipe . '_1')
                    ->where('DosenID', $info->DosenCID)
                    ->where('ParameterID',$v->ParameterID)
                    ->update(['ParamValue' => $n]);
                }
            }
            return redirect()->route('seminardetail', ['id' => $request->seminar_id]);
            //return $values;
        }
        return redirect()->action('HomeController@index');
    }
    
}
