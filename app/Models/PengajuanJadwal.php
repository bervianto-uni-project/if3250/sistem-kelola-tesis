<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PengajuanJadwal extends Model
{
    protected $table = 'pengajuan_jadwal';

    protected $fillable = [
        'ID', 'jadwal_id', 'Tanggal', 'Waktu_Awal', 'Waktu_Akhir', 'Jenis_Kegiatan', 'Keterangan', 'MahasiswaID', 'Approved',
    ];

    public $timestamps = false;
}
