<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bimbingan extends Model
{
    protected $table = 'bimbingan';

    public $timestamps = false;

    protected $fillable = [
        'Tanggal', 'MahasiswaID', 'Catatan', 'Rencana'
    ];

}
