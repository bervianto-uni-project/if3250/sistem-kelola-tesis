<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParameterPenilaian extends Model
{
	protected $table = 'parameter_penilaian';

    //public $primaryKey = 'ParameterID';

    public $timestamps = false;

    protected $fillable = [
        'NamaParameter', 'Deskripsi_L', 'Deskripsi_M', 'Deskripsi_K'
    ];
}
