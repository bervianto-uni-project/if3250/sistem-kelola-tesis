<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $table = 'mahasiswa';

    public $primaryKey = 'NIM';

    public $timestamps = false;

    public $incrementing = false;

}
