<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeminarDosen extends Model
{
    protected $table = 'seminardosen';

    public $primaryKey = 'SeminarID';

    public $timestamps = false;
}
