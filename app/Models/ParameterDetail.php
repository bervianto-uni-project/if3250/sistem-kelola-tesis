<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParameterDetail extends Model
{
    protected $table = 'parameter_detail';

    //public $primaryKey = 'ParameterID';

    public $timestamps = false;
}
