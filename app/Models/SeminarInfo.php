<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeminarInfo extends Model
{
    protected $table = 'seminar_info';

    protected $fillable = [
        'SeminarID', 'Tipe', 'Nilai', 'Active',
    ];

    public $primaryKey = 'SeminarID';

    public $timestamps = false;
}
