<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    protected $table = 'dosen';

    public $primaryKey = 'NIP';

    public $timestamps = false;

	public $incrementing = false;
}
